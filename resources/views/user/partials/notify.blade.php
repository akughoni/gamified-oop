@php
$success = session('success') ?? null;
$failed = session('failed') ?? null;
@endphp
<script>
    @if($success)
        $.notify({
            // options
            title: "Success",
            message: '{{ $success }}',
        },{
            // settings
            element: 'body',
            type: 'success',
            allow_dismiss: true,
            placement: {
                from: "top",
                align: "right"
            },
            timer: 1000,
            delay:1200,
        });
    @endif
    @if($failed)
        $.notify({
            // options
            title: "Failed",
            message: '{{ $failed }}',
        },{
            // settings
            element: 'body',
            type: 'danger',
            allow_dismiss: true,
            placement: {
                from: "top",
                align: "right"
            },
            timer: 1000,
            delay:1200,
        });
    @endif
</script>