<!-- Navigation-->
<nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top border-bottom" id="mainNav">
  <div class="container">
    <a class="navbar-brand js-scroll-trigger" href="#page-top">Gamified OOP</a>
    <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded"
      type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
      aria-expanded="false" aria-label="Toggle navigation">
      Menu
      <i class="fas fa-bars"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item mx-0 mx-lg-1"><a
            class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger {{ (request()->is("/dashboard") ? 'active' : '') }}"
            href="{{ route("user.dashboard.index") }}">Dasbor</a>
        </li>
        <li class="nav-item mx-0 mx-lg-1"><a
            class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger {{ (request()->is("course*") ? 'active' : '') }}"
            href="{{ route("user.course.index") }}">Materi</a></li>
        <li class="nav-item mx-0 mx-lg-1"><a
            class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger {{ (request()->is("mission*") ? 'active' : '') }}"
            href="/mission">Misi</a></li>
        <li class="nav-item mx-0 mx-lg-1"><a
            class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger {{ (request()->is("leaderboard*") ? 'active' : '') }}"
            href="/leaderboard">Rangking</a></li>
      </ul>
    </div>
    <div class="form-group">
      <a href="{{ route("user.logout") }}">
        <button type="button" class="btn btn-danger btn-block" data-toggle="tooltip" data-placement="bottom"
          title="Sign out"><i class="fas fa-power-off"></i></button>
      </a>
    </div>
  </div>
</nav>
