@extends('user.app')
@section('css')

@endsection

@section('content')
<!-- Dashboard Section-->
<section class="page-section text-secondary mb-0 mt-n3" id="about">
  <div class="container">
    <!-- Profile Section Content-->
    <div class="d-flex justify-content-center">
      <div class="d-inline-flex justify-content-center rounded p-3">
        <div class="col-lg-5 ml-auto mr-3">
          <div class="profile-image">
            <img src="{{ asset(Storage::url(auth()->user()->image)) }}" class="mx-auto d-block" alt="Gamified OOP">
          </div>
          <div class="d-flex justify-content-center mt-n4 profile-point">
            <div class="m-1">
              <a href="#" class="btn btn-xm font-weight-bold">Level : {{ auth()->user()->level->level }}</a>
              <a href="#" class="btn btn-xm font-weight-bold ml-auto">Skor : {{ $point }}</a>
            </div>
          </div>
        </div>
        <div class="col-lg-8 ml-5 mr-5 mt-3">
          <h1 class="profile-heading text-left text-secondary">Selamat datang kembali!</h1>
          <h1 class="profile-name text-left text-secondary">{{ auth()->user()->fullname }}</h1>
          <hr>
          <p class="profile-content text-left text-primary">{{ auth()->user()->phone }}</p>
          <p class="profile-content text-left text-primary">{{ auth()->user()->class }}</p>
          <div class="form-group mt-4">
              <a href="{{ route('user.course.index') }}">
                <button type="submit" class="btn m-2 btn-blue btn-block"> Lanjutkan Materi</button>
              </a>
              <a href="{{ route('user.mission.index') }}">
                <button type="submit" class="btn m-2 btn-blue btn-block"> Lanjutkan Misi </button>
              </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Certificate Section-->
@if (true)
{{--@if ($countCourseIsDone == $courseCount)--}}
<section class="mb-5 pb-5">
  <div class="container d-flex align-items-center flex-column">
    <h1 class="masthead-heading text-uppercase mb-0">Sertifikat</h1>
    <!-- Icon Divider-->
    <div class="divider-custom">
      <div class="divider-custom-line"></div>
      <div class="divider-custom-icon"><i class="fas fa-trophy"></i></div>
      <div class="divider-custom-line"></div>
    </div>
    <!-- Masthead Subheading
                <p class="masthead-subheading font-weight-light mb-0">Graphic Artist - Web Designer - Illustrator</p>-->
  </div>
  <div class="d-flex justify-content-center">
    <div class="content">
      <div class="ctfCard">
        <div class="firstinfo"><img src="https://bootdey.com/img/Content/avatar/avatar6.png" />
          <div class="profileinfo">
            <h1>{{ auth()->user()->fullname }}</h1>
            <h3>OOP developer</h3>
            <p class="bio">Telah menyelesaikan semua materi OOP dengan hasil yang memuaskan.</p>
          </div>
        </div>
      </div>
      <a href="{{ route('user.certificate') }}">
        <div class="badgescard">
          <span class="fa fa-download"></span>
        </div>
      </a>
    </div>
  </div>
</section>
@endif
<!-- Achievement Section-->
<section class="mb-5 pb-5">
  <div class="container border rounded">
    <h1 class="profile-heading m-3 text-left">Lencana Kamu</h1>
    <hr>

    <div class="d-flex justify-content-center flex-wrap">
      @forelse($badges as $badge)
      <div class="card">
        <div class="additional">
          <div class="m-3">
            <p class="profile-content">Lencana</p>
            <p class="heading">{{ $badge->title }}</p>
            <p class="describe">{{ $badge->description }}</p>
          </div>
          <div class="stats">
            <div>
              <i class="fa fa-check-circle"></i>
            </div>
            <div>
              <p class="vertical-center">Tercapai</p>
            </div>
          </div>
        </div>
        <div class="user-card">
          <img src="{{ asset('user/assets/img/badged-active.png') }}" class="img-fluid">
        </div>
      </div>
      @empty

      @endforelse
      @forelse ($badgesNotTaken as $item)
      <div class="card">
        <div class="additional">
          <div class="m-3">
            <p class="profile-content">Lencana</p>
            <p class="heading">{{ $item->title }}</p>
            <p class="describe">{{ $item->description }}</p>
          </div>
        </div>
        <div class="user-card">
          <img src="{{ asset('user/assets/img/badges.png') }}" class="img-fluid">
        </div>
      </div>
      @empty

      @endforelse

    </div>
  </div>
</section>
@endsection

@section('js')

@endsection
