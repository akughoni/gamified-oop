@extends('user.app')
@section('css')

@endsection

@section('title-page')

@endsection

@section('content')
<!-- Leaderboard Section-->
<section class="page-section text-secondary mb-0 mt-n3" id="about">
  <div class="container">
    <div class="d-flex justify-content-center">
      <div class="d-inline-flex justify-content-center rounded p-3">
        <div class="col-5">
          <img src="{{ asset('user/assets/img/award.png') }}" class="img-fluid">
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Rank Section-->
<section class="mt-n3 mb-5 pb-5">
  <div class="container">

    <!-- Rank Card-->
    <div class="rankCard m-auto">
      <div class="ribbon">
        <h4 class="text-center text-white">Para Pemain dengan Pencapaian Tertinggi.</h4>
      </div>
      <div class="mx-auto pt-3">
        @foreach ($users as $user)
        <div class="cardList">
          <div class="d-flex flex-row" style="align-items: center;">
            <div class="p-2">
              <h1>{{ $loop->iteration }}.</h1>
            </div>
            <div class="p-2">
              <img src="{{ asset(Storage::url($user->image)) }}" class="img-fluid" width="80px"
                style="border-radius: 50%;">
            </div>
            <div class="p-2">
              <h4>{{ $user->username }}</h4>
            </div>
            <div class="ml-auto p-2">
              <a href="#" class="btn btn-xm font-weight-bold">{{ $user->points->sum("point") }}/EXP</a>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>

  </div>
</section>
@endsection

@section('js')

@endsection
