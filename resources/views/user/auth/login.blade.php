<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>GamOOP - Easy Way To Learn OOP</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="{{ asset('user/assets/img/jigsaw-icon.png') }}" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet"
        type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="{{ asset('user/css/styles.css') }}" rel="stylesheet" />
    {{-- <link rel="stylesheet" href="{{ asset('admin/css/atlantis.min.css') }}"> --}}
</head>

<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top border-bottom" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top">Gamified OOP</a>
            <button
                class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded"
                type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
                aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                            data-toggle="modal" data-target="#exampleModalCenter">Daftar</a></li>
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                            href="#">Tentang</a></li>
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                            href="#">Bantuan</a></li>
                </ul>
            </div>
        </div>
    </nav>
    {{-- Modal --}}
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body container">
                    <div class="row justify-content-center">
                        <div class="col-10">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="card-title mt-1 text-center">Buat Profilmu</h4>
                            <div class="divider-custom">
                                <div class="divider-custom-line"></div>
                                <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                <div class="divider-custom-line"></div>
                            </div>
                            <form id="form-regitser" method="POST" action="{{ route("user.register.proses") }}">
                                @csrf
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                                    </div>
                                    <input name="fullname" class="form-control" value="{{ old("fullname") }}"
                                        placeholder="Full name" type="text">
                                </div>
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                                    </div>
                                    <input name="username" class="form-control" value="{{ old("username") }}"
                                        placeholder="Username" type="text">
                                </div>
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                                    </div>
                                    <input name="phone" class="form-control" value="{{ old("phone") }}"
                                        placeholder="Phone number" type="text">
                                </div>
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-graduation-cap"></i> </span>
                                    </div>
                                    <select name="class" class="form-control">
                                        <option selected=""> Class</option>
                                        <option>XI-RPL A</option>
                                        <option>XI-RPL B</option>
                                        <option>XI-RPL C</option>
                                    </select>
                                </div>
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                                    </div>
                                    <input name="password" value="{{ old("password") }}" class="form-control"
                                        placeholder="Create password" type="password">
                                </div>
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                                    </div>
                                    <input name="password_confirmation" value="{{ old("password_confirmation") }}"
                                        class="form-control" placeholder="Repeat password" type="password">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        Create Account </button>
                                </div>
                                <p class="text-center">Have an account? <a href="">Log In</a> </p>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- About Section-->
    <section class="page-section bg-secondary text-white mb-0 mt-5" id="about">
        <div class="container">
            <!-- About Section Content-->
            <div class="row">
                <div class="col-lg-5 ml-auto">
                    <!-- About Section Heading-->
                    <h1 class="page-section-heading text-left text-uppercase text-white">ENJOY LEARNING.</h1>
                    <h2 class="page-section-heading text-left text-uppercase text-white">LIKE GAMING.</h2>
                    <!-- Icon Divider-->
                    <div class="divider-custom divider-light">
                        <div class="divider-custom-line"></div>
                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                        <div class="divider-custom-line"></div>
                    </div>
                    <form id="form-login" action="{{ route("user.login.proses") }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <input type="text" name="username" value="{{ old("username") }}" class="form-control"
                                    id="exampleInputEmail1" placeholder="Enter Username">
                            </div>
                            @error("username")
                            <small id="emailHelp" class="form-text text-light">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-eye-slash" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <input type="password" name="password" value="{{ old("password") }}"
                                    class="form-control" id="exampleInputPassword1" placeholder="Password">
                            </div>
                            @error("password")
                            <small id="emailHelp" class="form-text text-light">{{ $message }}</small>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-xl btn-outline-light">Login</button>
                    </form>
                </div>
                <div class="col-lg-7 mr-auto">
                    <img src="{{ asset('user/assets/img/landing-page.png') }}" class="img-fluid" alt="Gamified OOP">
                </div>
            </div>
            <!-- About Section Button-->
            <div class="text-center mt-4">
            </div>
        </div>
    </section>
    <!-- Footer-->
    <div class="copyright py-4 text-center text-white">
        <div class="container"><small>Copyright © GamOOP 2020</small></div>
    </div>
    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
    <div class="scroll-to-top d-lg-none position-fixed">
        <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i
                class="fa fa-chevron-up"></i></a>
    </div>
    <!-- Bootstrap core JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <!-- Contact form JS-->
    <script src="{{ asset('user/assets/mail/jqBootstrapValidation.js') }} "></script>
    <script src="{{ asset('user/assets/mail/contact_me.js') }}"></script>
    <!-- Core theme JS-->
    <script src="{{ asset('user/js/scripts.js') }}"></script>
    <script src="{{ asset('admin/js/plugin/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
    @include('user.partials.notify')
    <script>
        // $("#form-register").su
        $("#form-register").submit(function (e) { 
            e.preventDefault();
            console.log("test");
        });
    </script>
</body>

</html>