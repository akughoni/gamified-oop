<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <title>GamOOP - Easy Way To Learn OOP</title>
  <!-- Favicon-->
  <link rel="icon" type="image/x-icon" href="{{ asset('user/assets/img/jigsaw-icon.png') }}" />
  <!-- Font Awesome icons (free version)-->
  <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
  <!-- Google fonts-->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet"
    type="text/css" />
  <!-- Core theme CSS (includes Bootstrap)-->
  <link href="{{ asset('user/css/styles.css') }}" rel="stylesheet" />
</head>

<body id="page-top">
  <!-- Register Section-->
  <div class="container">
    <div class="rounded border bg-light m-3">
      <div>
        <button class="btn btn-light" onclick="window.location.href='/login'">
          <a class="fa fa-times" aria-hidden="true"></a>
        </button>
      </div>
      <article class="card-body mx-auto bg-light" style="max-width: 400px;">
        <h4 class="card-title mt-1 text-center">Buat Profilmu</h4>
        <div class="divider-custom">
          <div class="divider-custom-line"></div>
          <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
          <div class="divider-custom-line"></div>
        </div>
        <form>
          <div class="form-group input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-user"></i> </span>
            </div>
            <input name="" class="form-control" placeholder="Full name" type="text">
          </div>
          <div class="form-group input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-user"></i> </span>
            </div>
            <input name="" class="form-control" placeholder="Username" type="text">
          </div>
          <div class="form-group input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
            </div>
            <select class="custom-select" style="max-width: 120px;">
              <option selected="">+62</option>
            </select>
            <input name="" class="form-control" placeholder="Phone number" type="text">
          </div>
          <div class="form-group input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-graduation-cap"></i> </span>
            </div>
            <select class="form-control">
              <option selected=""> Class</option>
              <option>XI-RPL A</option>
              <option>XI-RPL B</option>
              <option>XI-RPL C</option>
            </select>
          </div>
          <div class="form-group input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
            </div>
            <input class="form-control" placeholder="Create password" type="password">
          </div>
          <div class="form-group input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
            </div>
            <input class="form-control" placeholder="Repeat password" type="password">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block"> Create Account </button>
          </div>
          <p class="text-center">Have an account? <a href="">Log In</a> </p>
        </form>
      </article>
    </div> <!-- card.// -->
  </div>
  <!--container end.//-->
  <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
  <div class="scroll-to-top d-lg-none position-fixed">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i
        class="fa fa-chevron-up"></i></a>
  </div>
  <!-- Bootstrap core JS-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
  <!-- Third party plugin JS-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
  <!-- Contact form JS-->
  <script src="{{ asset('user/assets/mail/jqBootstrapValidation.js') }} "></script>
  <script src="{{ asset('user/assets/mail/contact_me.js') }}"></script>
  <!-- Core theme JS-->
  <script src="{{ asset('user/js/scripts.js') }}"></script>
</body>

</html>