@extends('user.app')
@section('css')

@endsection

@section('title-page')

@endsection

@section('content')
    <!-- Dashboard Section-->
    <section class="page-section text-secondary mb-0 mt-n3" id="about">
        <div class="container">
            <!-- Profile Section Content-->
            <div class="d-flex justify-content-center">
                <div class="d-inline-flex justify-content-center rounded p-3">

                    <div class="col-auto ml-5 mr-3 mt-3">
                        <h1 class="profile-name text-left text-secondary">Halo, {{ auth()->user()->fullname }}</h1>
                        <h1 class="profile-heading text-left text-secondary">Selamat datang kembali!</h1>
                        <hr>
                        <p class="profile-content text-left text-primary">Selesaikan semua level untuk menjadi yang terbaik!</p>
                    </div>

                    <div class="col-lg-5 ">
                        <div class="d-flex justify-content-center profile-point vertical-center">
                            <div class="mx-auto">
                                <a href="#" class="btn btn-xm font-weight-bold ml-auto">Skor kamu : {{ $point }}</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- Mission Section-->
    <section class="mb-5 pb-5">
        <div class="container border rounded ctnLevel">

            <!-- Mission Card-->
            <div class="container p-4">
                <div class="d-flex justify-content-center flex-wrap">
                    @forelse ($user->takeMission as $take)
                        <a href="{{ route("user.mission.show", ["id" => $take->mission_id]) }}">
                            <div class="m-3">
                                <div class="cardMission">
                                    <div class="row bg-white align-items-center justify-content-center pt-2">
                                        <div class="col-sm-4 text-center">
                                            @if($take->is_done == 1)
                                                <img src="{{ asset('/user/assets/img/achieved_mission.jpg') }}"
                                                     class="img-fluid">
                                            @else
                                                <img src="{{ asset('/user/assets/img/mission-flag.png') }}"
                                                     class="img-fluid">
                                            @endif
                                        </div>
                                        <div class="col-sm-12">
                                            <p class="text-center">{{ $take->mission->title }}</p>
                                        </div>
                                    </div>
                                    <div class="detail p-3">
                                        <p class="text-white text-center">{{ $take->mission->description }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @empty

                    @endforelse
                    @forelse ($missionsNotTaken as $item)
                        {{-- <a href="{{ route("user.mission.show", ["id" => $item->id]) }}"> --}}
                        <div class="m-3">
                            <div class="cardMission bg-gray-light">
                                <div class="row bg-white align-items-center justify-content-center pt-2">
                                    <div class="col-sm-4 text-center">
                                        <img src="{{ asset('user/assets/img/grey-lock.png') }}"
                                             class="img-fluid rounded-circle">
                                    </div>
                                    <div class="col-sm-12">
                                        <p class="text-center">{{ $item->title }}</p>
                                    </div>
                                </div>
                                <div class="detail p-3">
                                    <p class="text-white text-center">{{ $item->description }}</p>
                                </div>
                            </div>
                        </div>
                        {{-- </a> --}}
                    @empty
                    @endforelse
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')

@endsection
