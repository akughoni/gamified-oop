@extends('user.app')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('user/css/codemirror.css') }}">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
    <style>
        .modal-dialog {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            margin: auto;
            width: 400px;
            height: 200px;
        }
        .wrap {
            position: relative;
            height: 100%;
            background: #eee;
            padding: 20px;
            box-sizing: border-box;
        }

        .border {
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            border: 1px solid #999;
            position: relative;
            height: 100%;
            padding: 1px;
        }

        .CodeMirror {
            height: 25rem;
            background: white;
        }
    </style>
@endsection

@section('title-page')

@endsection

@section('content')
    <!-- Level Section-->
    <section class="page-section" id="contact">
        <div class="container">
            <div class="row bg-transparent rounded">
                <div class="col-lg-10 mx-auto">
                    <h3 class="text-center text-secondary mb-1">{{ $mission->title }}</h3>
                    <h6 class="text-center text-secondary mb-3">{{ $mission->description }}</h6>
                </div>

                <div class="col-12">
                    <div class="row">
                        <div class="col-6 border rounded bg-light px-3 py-2 text-dark">
                            <div class="bg-white px-4 py-3 h-100 w-100 border rounded">
                                <h5 class="text-left text-secondary mb-3">Mission</h5>
                                <div>
                                    {!! $mission->content !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-6 border rounded bg-light py-4 h-100">
                            <div>
                                <div class=wrap>
                                    <div class=border><textarea id="code"></textarea></div>
                                </div>

                                <div class="form-group mt-4 mx-3">
                                    <button id="submit" class="btn btn-primary px-5 py-2">Submit</button>
                                </div>
                            </div>
                            <div class="col-lg-12 my-3 mx-auto">
                                <h3 class="text-center text-secondary mb-1">Result</h3>
                            </div>
                            <div id="loading" class="col-12 absolute text-center d-none my-5">
                                <div class="flex justify-content-center">
                                    <div class="spinner-border text-secondary" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                            </div>
                            <div id="result" class="col-12 justify-content-center d-none">
                                <div id="divResult">
                                </div>
                                <div class="border rounded px-3 py-3">
                                    <p id="textResult"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('user/js/codemirror.js') }}"></script>
    <script>
        function showPleaseWait() {
            if (document.querySelector("#pleaseWaitDialog") == null) {
                var modalLoading = `<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" role="dialog">
                                    \
                                    <div class="modal-dialog">\
                                      <div class="modal-content">\
                                        <div class="modal-header text-center">\
                                          <h4 class="modal-title">Please wait...</h4>\
                                        </div>\
                                        <div class="modal-body text-center">\
                                          <div class="spinner-border text-secondary" role="status">
                                            <span class="sr-only"></span>
                                          </div>
                                        </div>\
                                      </div>\
                                    </div>\
                                  </div>`;
                $(document.body).append(modalLoading);
            }

            $("#pleaseWaitDialog").modal("show");
        }

        function hidePleaseWait() {
            $("#pleaseWaitDialog").modal("hide");
        }

        function showLoading() {
            $("#loading").removeClass("d-none");
            $("#loading").addClass("d-block");
            $("#result").removeClass("d-block");
            $("#result").addClass("d-none");
        }

        function hideLoading() {
            $("#loading").removeClass("d-block");
            $("#loading").addClass("d-none");
            $("#result").removeClass("d-block");
            $("#result").addClass("d-block");
        }

        function resetClassResult() {
            $("#divResult").removeAttr('class');
            $("#textResult").removeAttr('class');
        }

        $(document).ready(function () {

            var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
                lineNumbers: true,
                tabSize: 2,
                // viewportMargin: Infinity,
            });
            // editor.setSize('100%', '100%');
            const proxy = "https://cors-anywhere.herokuapp.com/";

            $("#submit").click(function (e) {
                e.preventDefault();
                var code = editor.getValue();
                showPleaseWait();
                var request = $.ajax({
                    type: "POST",
                    url: proxy + "https://ide.geeksforgeeks.org/main.php",
                    headers: {
                        Accept:"application/json",
                    },
                    data: {
                        lang: 'java',
                        code: code,
                        save: false,
                        input: null
                    },
                    dataType: "JSON",
                    success: function (response) {
                        showLoading();
                        checkResult(response.sid);
                    }
                });
            });

            function checkResult(id) {
                var request = $.ajax({
                    type: "POST",
                    url: proxy + "https://ide.geeksforgeeks.org/submissionResult.php",
                    data: {
                        sid: id,
                        requestType: "fetchResults"
                    },
                    dataType: "JSON",
                    success: function (response) {
                        if (response.status == "IN-QUEUE") {
                            setTimeout(checkResult(id), 1000);
                        } else {
                            hidePleaseWait();
                            hideLoading();
                            showResult(response);
                        }
                    }
                });
            }

            function showResult(response) {
                var divResult = $("#divResult");
                var textResult = $("#textResult");
                if (response.compResult == "F") {
                    resetClassResult();
                    alertify.alert().set('message', response.cmpError).show().setHeader('<p class="text-center">Error</p>');
                    divResult.addClass("alert alert-danger col-12");
                    divResult.html("Error");
                    textResult.html(response.cmpError);
                } else {
                    resetClassResult();
                    divResult.addClass("alert alert-success col-12");
                    alertify.alert().set('message', response.output).show().setHeader('<p class="text-center">' + response.status + '</p>');
                    divResult.html(response.status);
                    textResult.html(response.output);
                    storeTakeMission();
                }
            }

            function storeTakeMission() {
                //
                var data = {
                    "user_id": "{{ auth()->user()->id }}",
                    "mission_id": "{{ request()->id }}"
                }

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ route("user.mission.take") }}',
                    type: "POST",
                    data: data,
                    dataType: "JSON",
                    success: function (response) {
                        //
                    }
                })
            }
        });


    </script>
@endsection
