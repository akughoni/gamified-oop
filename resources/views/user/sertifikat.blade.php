<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Favicon-->
  <link rel="icon" type="image/x-icon" href="{{ asset('user/assets/img/jigsaw-icon.png') }}" />
  <!-- Font Awesome icons (free version)-->
  <!-- Google fonts-->
  {{-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet"
  type="text/css" /> --}}
  <!-- Core theme CSS (includes Bootstrap)-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
  {{-- <link href="" rel="stylesheet" /> --}}
  {{-- <link href="{{ $data['link_css'] }}" rel="stylesheet" //> --}}
</head>

<body>
  <div class="m-5">
    <div id="pdf">
      <div class="bg-primary w-100 px-5 py-5">
        <h5 class="text-white pb-3"><img src="{{ asset('user/assets/img/jigsaw-icon.png') }}" class="col-1">
          Gamified-OOP
          Apps</h5>
        <h1 style="font-size: 3rem;" class="text-white font-weight-bold">Sertfikat Penghargaan</h1>
      </div>
      <div class="bg-white  h-100 row px-5 py-5">
        <div class="col-4">
          <img class="img-fluid" src="{{ $link_gambar }}" alt="trophy.png">
        </div>
        <div class="col-8 mt-5">
          <h2 class="text-dark font-weight-bold mb-5 text-uppercase">{{ auth()->user()->fullname }}</h2>
          <p style="font-size: 1.2rem;" class="mb-5 font-weight-normal text-dark">
            Telah menyelesaikan seluruh level dalam Materi Pemrograman Berorientasi Objek dengan skor {{ auth()->user()->points->sum('point') }}
          </p>
          <h5 class="text-dark font-weight-bold mb-4">Pemrograman Java Berorientasi Objek</h5>

          <p class="text-dark font-weight-bold"><span class="fa fa-check-circle mr-2"></span> Dicetak pada: {{ now() }}</p>

          <p class="text-dark font-weight-bold"><span class="fa fa-trophy mr-2"></span> Skor:
            {{ auth()->user()->points->sum('point') }}</p>
        </div>
      </div>
      <div class="w-100 py-3 text-right px-5">
        <p class="text-dark">Gamified-oop.com</p>
      </div>
    </div>
  </div>
  <div class="text-center px-2">
    <button class="btn btn-success px-3 py-1" onclick="generatePDF()">Save</button>
  </div>
  <script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
  <script>
    function generatePDF() {
    // Choose the element that our invoice is rendered in.
    const element = document.getElementById("pdf");
    var opt = {
      margin: 0,
      filename: 'Certificate-OOP.pdf',
      image: { type: 'jpeg', quality: 0.98 },
      html2canvas: { scale: 2 },
      jsPDF: { unit: 'in', format: 'a4', orientation: 'landscape' }
    };
    // Choose the element and save the PDF for our user.
    html2pdf()
      .set(opt)
      .from(element)
      .save();
    }
  </script>
  <!-- Bootstrap core JS-->
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> --}}
  {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script> --}}
</body>

</html>
