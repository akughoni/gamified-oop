<div class="d-flex justify-content-center row">
  @foreach ($quiz as $key => $item)
  <div class="col-md-8 col-lg-8">
    <div class="border">
      <div class=" bg-white p-3 border-bottom">
        <div class="d-flex flex-row justify-content-between align-items-center" id="{{ $item->id }}"
          number="{{ $page ?? 1 }}">
          <h5 class="mt-1 ml-2">{!! $item->question !!}</h5>
        </div>
      </div>
      <div class="bg-white p-3 border-bottom">
        <div class="form-check my-3">
          <input class="form-check-input" type="radio" name="{{ $item->id }}" id="a" value="a">
          <label class="form-check-label" for="a">{!! $item->answer_a !!}</label>
        </div>
        <div class="form-check my-3">
          <input class="form-check-input" type="radio" name="{{ $item->id }}" id="b" value="b">
          <label class="form-check-label" for="b">{!! $item->answer_b !!}</label>
        </div>
        <div class="form-check my-3">
          <input class="form-check-input" type="radio" name="{{ $item->id }}" id="c" value="c">
          <label class="form-check-label" for="c">{!! $item->answer_c !!}</label>
        </div>
        <div class="form-check my-3">
          <input class="form-check-input" type="radio" name="{{ $item->id }}" id="d" value="d">
          <label class="form-check-label" for="d">{!! $item->answer_d !!}</label>
        </div>
      </div>
      {{-- Paggination --}}
      <div class="d-flex flex-row justify-content-center align-items-center p-3 bg-white">
        {{ $quiz->onEachSide($count)->links('vendor.quiz') }}
      </div>
    </div>
  </div>
  @endforeach
</div>