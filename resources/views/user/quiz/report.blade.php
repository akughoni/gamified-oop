<div class="row justify-content-center align-content-center h-100">
  <div class="col-12 my-3 text-center">
    <h1 class="text-secondary">Skor kuis : {{ $score }}</h1>
  </div>
  <div class="col-12 my-2 text-center">
    <h6>{{ $message }}</h6>
  </div>
  <div class="col-12 text-center my-5">
    <a href="{{ route('user.course.index') }} " class="text-decoration-none">
      <span class="py-3 px-5 text-center bg-primary text-white">
        Back to Course
      </span>
    </a>
  </div>
</div>
