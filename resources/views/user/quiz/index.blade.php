@extends('user.app')
@section('css')
<style>
  html,
  body {
    height: 100%;
  }

  .fill {
    /* height: 100%; */
    min-height: 100%;
  }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css" />
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
@endsection

@section('title-page')

@endsection

@section('content')
<div class="page-section">
  <div class="row justify-content-center align-content-center">
    <div class="container mt-5" id="question">
      @include('user.quiz.question')
    </div>
  </div>
</div>


@endsection

@section('js')
<script>
  let urlGetData = "{{ route('user.quiz.index',['courseId' => $courseId]) }}";
  let urlSubmit = "{{ route('user.quiz.submit',['courseId' => $courseId]) }}";
  let data = [];
  
  function getData(url = urlGetData) {
  $.ajax({
    method: 'GET',
    url: url,
    success: function (res) {
      $('#question').html(res);
    }
    }).done(() => {
      checked_answer();
    });
  }
  
  $(document).on('click', 'nav .pagination .link', function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    getData(url);
  });

      $(document).ready(function () {
        // push user answer on array obj
       $(document).on('change','input[type="radio"]',function (e) { 
            e.preventDefault();
            const radio = $('input[type="radio"]');
            const checked = radio.filter(function(item,index, arr){ //find html radio button on checked
                return $(this).prop('checked');
            })
            const questionId = checked.attr('name');
            const number = $('#'+questionId).attr('number');
            const value = checked.val();
            const answer = {
                id : questionId,
                value : value,
                number : number
            }

            if(data.length == 0){
                data.push(answer);
            }else{
                const found = data.some(item => item.id === questionId); //return true or false from condition
                if(found){
                    // make new array obj with update value where data.id == questionId
                    data = data.map((item) => {
                        if(item.id === questionId){
                            item.value = value;
                            return item
                        }
                        return item;
                    });
                }else{
                    data.push(answer);
                }
            }
            console.log(data);
       });
    });

    function checked_answer(){
        const radio = $('input[type="radio"]');
        const currentId = $(radio).attr('name');
        let input ={};
        let value =null;
        let element = data.filter((item) => item.id === currentId );
        element.forEach(el => {
            value = el.value;
        })
        selector = 'input[value="'+value+'"]';
        $(selector).prop('checked', true);
    }

    $(document).on('click','#submit', (e) => {
        if(data.length == '{{ $count }}'){
            alertify.confirm('Apakah anda yakin submit jawaban anda?', 
            function(e){ 
                $.ajax({
                    type: "POST",
                    url: urlSubmit,
                    data: {
                        _token: "{{ csrf_token() }}",
                        data : data
                    },
                    success: function(res){
                        $("#question").html(res);
                        $('#title_page').html("Hasil Pretest!");
                        $('#desc_page').html("Berikut adalah report hasil pretest yang sudah kamu lakukan, kamu dapat melihat mana jawaban yang salah atau benar!!");
                    }
                });
            }).setHeader('<p></p>');
        }else{
            alertify.alert().set('message', 'Anda Belum Menjawab Keseluruhan Soal! Silakan Periksa Kembali Jawaban Anda.')
                            .show()
                            .setHeader('<p></p>'); 
        }

    });

</script>
@endsection