@extends('user.app')
@section('css')

@endsection

@section('title-page')

@endsection

@section('content')
<!-- Dashboard Section-->
<section class="page-section text-secondary mb-0 mt-n3" id="about">
  <div class="container">
    <!-- Profile Section Content-->
    <div class="d-flex justify-content-center">
      <div class="d-inline-flex justify-content-center rounded p-3">

        <div class="col-auto ml-5 mr-3 mt-3">
          <h1 class="profile-name text-left text-secondary">Halo, {{ auth()->user()->fullname }}</h1>
          <h1 class="profile-heading text-left text-secondary">Selamat datang kembali!</h1>
          <hr>
          <p class="profile-content text-left text-primary">Selesaikan semua materi untuk menjadi yang terbaik!</p>
        </div>

        <div class="col-lg-5 ">
          <div class="d-flex justify-content-center profile-point vertical-center">
            <div class="mx-auto">
              <a href="#" class="btn btn-xm font-weight-bold ml-auto">Skor kamu :
                {{ auth()->user()->points->sum("point") }}</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Level Section-->
<section class="mb-5 pb-5">
  <div class="container border rounded ctnLevel">
    <!-- Level Section-->
    <div class="">
      <div class="d-flex justify-content-center flex-wrap p-4">
        @foreach($user->coursesTaken as $itemOpen)
        <div class="my-4 mx-4 py-5">
          <a href="{{ route("user.course.show", ["courseId" => $itemOpen->id]) }}" class="text-decoration-none">
            <div class="cardLevel h-100 ">
              <div class="firstinfo">
                <img src="{{ asset(Storage::url($itemOpen->image)) }}" style="margin-top:-65px"
                  class="img-fluid img-thumbnail rounded-circle border-0 mb-3">
              </div>
              <p class="text-center text-primary ">Level {{ $itemOpen->level }}</p>
              <i class="text-center">{{ $itemOpen->title }}</i>
            </div>
          </a>
          <a class="text-decoration-none" href="{{ route("user.quiz.index", ["courseId" => $itemOpen->id]) }}">
            <div class="btn btn-primary py-3 col text-center">
              <b class="text-center">Buka Kuis!</b>
            </div>
          </a>
        </div>
        @endforeach

        @foreach ($courseNotTaken as $itemLock)
        <div class="my-4 mx-4 py-5">
          <div class="cardLevel h-100 p-3">
            <div class="firstinfo">
              <img src="{{ asset('user/assets/img/grey-lock.png') }}" style="margin-top:-65px"
                class="img-fluid img-thumbnail rounded-circle border-0 mb-3">
            </div>
            <p class="text-center">Level {{ $itemLock->level }}</p>
            <i class="text-center">{{ $itemLock->title }}</i>
          </div>
        </div>
        @endforeach

      </div>
    </div>

  </div>
</section>
@endsection

@section('js')

@endsection
