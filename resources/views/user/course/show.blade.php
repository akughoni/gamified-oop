@extends('user.app')
@section('css')

@endsection

@section('title-page')

@endsection

@section('content')
    <!-- Level Section-->
    <section class="page-section" id="contact">
        <div class="container">
            <!-- Contact Section Heading-->
            <!-- Contact Section Form-->
            <div class="row bg-transparent shadow rounded">
                @foreach ($stage as $item)
                    <div class="col-lg-8 mx-auto">
                        <h5 class="text-center text-secondary mt-4 mb-1">Stage {{ $stage->currentPage() }}:</h5>
                        <h6 class="text-center text-secondary mb-3">{{ $item->title }}</h6>
                        <div class="progress mb-3">
                            <div class="progress-bar" role="progressbar"
                                 style="width: {{ number_format($stage->currentPage()/$total * 100, 0) }}%;"
                                 aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                {{ number_format($stage->currentPage()/$total * 100, 0) }}%
                            </div>
                        </div>
                        <h6 class="text-left text-secondary mt-1 mb-1" id="stage-content"
                            have-access="@if(\App\Models\UserPoint::where([
                                            'user_id' => auth()->id(),
                                            'from'    => 'course',
                                            'from_id' => $item->course_id
                                        ])->get()->count()) 1 @else 0 @endif"
                            data-words="{{ str_word_count(strip_tags($item->content)) }}">
                            {!! $item->content !!}
                        </h6>
                        @if(!empty($item->video))
                            <h6 class="text-left text-secondary mt-1 mb-1"><br><br> Simak video berikut untuk lebih
                                memahami
                                konsep
                                tentang
                                OOP</h6>
                            <div class="embed-responsive embed-responsive-16by9 mt-3 mb-3">
                                <iframe class="embed-responsive-item" src="{{ $item->video }}" allowfullscreen></iframe>
                            </div>
                        @endif
                        <div class="form-group row justify-content-center mt-3">
                            {{-- <a href="C1_level1_stage2.html" class="btn btn-primary btn-xl" id="nextButton" type="button" disabled>
                              <span class="spinner-border spinner-border-sm mb-1" role="status" aria-hidden="true"></span>
                              <span id="txtButton">Load...</span>
                            </a> --}}
                            {{ $stage->onEachSide(1)->links("vendor.pagination.show-course") }}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            var content = $("#stage-content");
            var word_count = content.attr("data-words");
            var access = content.attr("have-access");
            var next_link = $("#link-next-btn").attr("href");
            $("#link-next-btn").removeAttr("href");
            var timeDelay = word_count * 0.3 * 1000;

            if(Boolean(Number(access)) == false){
                setTimeout(function () {
                    $("#link-next-btn").prop("href", next_link);
                    $('.spinner-border').removeClass('spinner-border');
                    $('#text-next-btn').text('Next');
                    $('#text-done-btn').text('Finish');
                    $("#done-btn").prop("href", '{{ route("user.course.done", ["courseId" => request()->courseId ]) }}');
                }, timeDelay);
            }else{
                $("#link-next-btn").prop("href", next_link);
                $('.spinner-border').removeClass('spinner-border');
                $('#text-next-btn').text('Next');
                $('#text-done-btn').text('Finish');
                $("#done-btn").prop("href", '{{ route("user.course.done", ["courseId" => request()->courseId ]) }}');
            }

        });
    </script>
@endsection
