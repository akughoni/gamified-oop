@if ($paginator->hasPages())
<nav>
    <div class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
        <li class="page-item disabled" aria-disabled="true">
            <span class="page-link text-grey">@lang('pagination.previous')</span>
        </li>
        @else
        <li class="page-item">
            <a class="link page-link text-secondary" href="{{ $paginator->previousPageUrl() }}"
                rel="prev">@lang('pagination.previous')</a>
        </li>
        @endif
        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
        <li class="page-item">
            <a class="link page-link text-secondary" href="{{ $paginator->nextPageUrl() }}"
                rel="next">@lang('pagination.next')</a>
        </li>
        @else
        <li class="page-item" aria-disabled="true">
            <button id="submit" class="page-link text-secondary">Submit</button>
        </li>
        @endif
    </div>
</nav>
@endif