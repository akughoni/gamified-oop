@if ($paginator->hasPages())
<nav class="">
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
        <li class="page-item disabled mx-1" aria-disabled="true">
            <a class="btn btn-light btn-xl" id="prev-btn" type="button" disabled>
                <span id="text-prev-btn">@lang('pagination.previous')</span>
            </a>
        </li>
        @else
        <li class="page-item mx-1">
            <a href="{{ $paginator->previousPageUrl() }}" class="btn btn-primary btn-xl" id="prev-btn" type="button">
                <span id="text-prev-btn">@lang('pagination.previous')</span>
            </a>
        </li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
        <li class="page-item mx-1">
            <a href="{{ $paginator->nextPageUrl() }}" id="link-next-btn" class="btn btn-primary btn-xl text-white">
                <span class="spinner-border spinner-border-sm mb-1" role="status" aria-hidden="true"></span>
                <span id="text-next-btn">Load...</span>
            </a>
        </li>
        @else
        <li class="page-item disabled mx-1" aria-disabled="true">
            <a id="done-btn" class="btn btn-primary btn-xl text-white">
                <span class="spinner-border spinner-border-sm mb-1" role="status" aria-hidden="true"></span>
                <span id="text-done-btn">Load...</span>
            </a>
        </li>
        @endif
    </ul>
</nav>
@endif