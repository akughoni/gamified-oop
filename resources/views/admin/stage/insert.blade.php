@extends("admin.app")

@section('css')
    <link rel="stylesheet" href="{{ asset("admin/summernote/summernote-bs4.css") }}">
@endsection

@section("content")
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Create New Stage</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="{{ route("admin.dashboard.index") }}">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route("admin.course.index") }}">Data</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Create Data</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route("admin.stage.store") }}" method="POST">
                                @csrf
                                <input type="hidden" name="course_id" value="{{ $course_id }}">
                                <div class="form-group @error(" title") has-error has-feedback @enderror">
                                    <label for="title">Course Title</label>
                                    <input type="text" class="form-control" name="title" value="{{ old("title") }}"
                                           id="title"
                                           placeholder="Title">
                                    @error("title")
                                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group @error(" content") has-error has-feedback @enderror">
                                    <label for="cotent">Content</label>
                                    <textarea name="content" id="content-text" rows="10">{{ old("content") }}</textarea>
                                    @error("content")
                                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group @error(" video") has-error has-feedback @enderror">
                                    <label for="video">URL Video Embed</label>
                                    <input type="text" class="form-control" name="video" value="{{ old("video") }}"
                                           id="video"
                                           placeholder="video">
                                    @error("video")
                                    <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Sumbit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('js')
    <script src="{{ asset("admin/summernote/summernote-bs4.js") }}"></script>
    <script>
        $(document).ready(function () {
            $('#content-text').summernote({
                height: 300,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ],
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete : function(target) {
                        deleteImage(target[0].src);
                    }
                }
            });
        });

        function uploadImage(image) {
            var data = new FormData();
            data.append("file", image);
            $.ajax({
                url: `{{ route("image.upload") }}`,
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "POST",
                success: function(url) {
                    $('#content-text').summernote("insertImage", url);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }

        function deleteImage(src) {
            $.ajax({
                data: {src : src},
                type: "POST",
                url: `{{ route("image.delete") }}`,
                cache: false,
                success: function(response) {
                    console.log(response);
                }
            });
        }
    </script>
@endsection
