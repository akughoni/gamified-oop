@extends("admin.app")
@section('css')
<link rel="stylesheet" href="{{ asset("admin/summernote/summernote-bs4.css") }}">
@endsection
@section("content")
<div class="content">
  <div class="page-inner">
    <div class="page-header">
      <h4 class="page-title">Data Mission</h4>
      <ul class="breadcrumbs">
        <li class="nav-home">
          <a href="{{ route("admin.dashboard.index") }}">
            <i class="flaticon-home"></i>
          </a>
        </li>
        <li class="separator">
          <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
          <a href="{{ route("admin.mission.index") }}">Data</a>
        </li>
        <li class="separator">
          <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
          <a href="#">Create Data</a>
        </li>
      </ul>
      {{--      <div class="ml-md-auto py-2 py-md-0">--}}
      {{--        <a href="#" class="btn btn-secondary btn-round">Add New Course</a>--}}
      {{--      </div>--}}
    </div>
    <form action="{{ route("admin.mission.update", ["id" => $mission->id]) }}" method="POST"
      enctype="multipart/form-data">
      <div class="row">
        <div class="col-md-7">
          <div class="card">
            <div class="card-body">
              @csrf
              <div class="form-group @error(" title") has-error has-feedback @enderror">
                <label for="title">Mission Title</label>
                <input type="text" class="form-control" name="title" value="{{ $mission->title ?? old("title") }}"
                  id="title" placeholder="Title">
                @error("title")
                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group @error(" description") has-error has-feedback @enderror">
                <label for="description">Short Description</label>
                <textarea class="form-control" name="description" id="description" cols="30"
                  rows="3">{{ $mission->description ?? old("description") }}</textarea>
                @error("description")
                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group @error(" content") has-error has-feedback @enderror">
                <label for="cotent">Content</label>
                <textarea name="content" id="content-text"
                  rows="10">{{ $mission->content ?? old("content") }}</textarea>
                @error("content")
                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                @enderror
              </div>
{{--              <div class="form-group">--}}
{{--                <button class="btn btn-primary" type="submit">Sumbit</button>--}}
{{--              </div>--}}
            </div>
          </div>
        </div>
        <div class="col-md-5">
          <div class="card">
            <div class="card-body">
              <div class="form-group @error(" badge_title") has-error has-feedback @enderror">
                <label for="badge_title">Badge Title</label>
                <input type="text" class="form-control" name="badge_title"
                  value="{{ $mission->badge->title ?? old("badge_title") }}" id="badge_title" placeholder="Title">
                @error("badge_title")
                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group @error(" badge_description") has-error has-feedback @enderror">
                <label for="badge_description">Short Description</label>
                <textarea class="form-control" name="badge_description" id="badge_description" cols="30"
                  rows="5">{{ $mission->badge->description ?? old("badge_description") }}</textarea>
                @error("badge_description")
                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group">
                <button class="btn btn-primary" type="submit">Sumbit</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

</div>
@endsection
@section('js')
<script src="{{ asset("admin/summernote/summernote-bs4.js") }}"></script>
<script>
  $(document).ready(function() {
    $('#content-text').summernote({
      height: 300,
        callbacks: {
            onImageUpload: function(image) {
                uploadImage(image[0]);
            },
            onMediaDelete : function(target) {
                deleteImage(target[0].src);
            }
        }
    });
  });

  function uploadImage(image) {
      var data = new FormData();
      data.append("file", image);
      $.ajax({
          url: `{{ route("image.upload") }}`,
          cache: false,
          contentType: false,
          processData: false,
          data: data,
          type: "POST",
          success: function(url) {
              $('#content-text').summernote("insertImage", url);
          },
          error: function(data) {
              console.log(data);
          }
      });
  }

  function deleteImage(src) {
      console.log(src)
      $.ajax({
          data: {src : src},
          type: "POST",
          url: `{{ route("image.delete") }}`,
          cache: false,
          success: function(response) {
              console.log(response);
          }
      });
  }
</script>
@endsection
