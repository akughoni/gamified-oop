@extends("admin.app")

@section("content")
<div class="content">
  <div class="page-inner">
    <div class="page-header">
      <h4 class="page-title">Data Mission</h4>
      <ul class="breadcrumbs">
        <li class="nav-home">
          <a href="{{ route("admin.dashboard.index") }}">
            <i class="flaticon-home"></i>
          </a>
        </li>
        <li class="separator">
          <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
          <a href="{{ route("admin.mission.index") }}">Data</a>
        </li>
        <li class="separator">
          <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
          <a href="#">Show Data</a>
        </li>
      </ul>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="col-12 py-2">
            <ul class="nav nav-pills nav-pills-icons nav-secondary nav-pills-no-bd" id="pills-tab-without-border"
              role="tablist">
              <li class="nav-item col-2 text-center">
                <a class="nav-link active" id="pills-mission-tab-nobd" data-toggle="pill" href="#detail-mission"
                  role="tab" aria-controls="pills-mission-nobd" aria-selected="true">
                  <i class="fas fa-code"></i>
                  Mission Detail
                </a>
              </li>
              <li class="nav-item col-2 text-center">
                <a class="nav-link" id="pills-stage-tab-nobd" data-toggle="pill" href="#list-stage" role="tab"
                  aria-controls="pills-stage-nobd" aria-selected="false">
                  <i class="fab fa-font-awesome-alt"></i>
                  Badge
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="tab-content mt-2 mb-3" id="show-mission">
          <div class="tab-pane fade show active" id="detail-mission" role="tabpanel"
            aria-labelledby="pills-mission-tab">
            <div class="card">
              <div class="card-header row">
                <div class="col">
                  <h4>Mission Detail</h4>
                </div>
                <div class="ml-md-auto mr-md-5">
                  <a href="{{ route("admin.mission.edit", ["id" => $mission->id]) }}"
                    class="btn btn-sm btn-warning btn-round">Update Mission</a>
                </div>
              </div>
              <div class="card-body">
                <form>
                  <div class="form-group row align-items-center">
                    <label for="" class="col-3">Mission Title</label>
                    <input type="text" class="form-control col-9" value="{{ $mission->title }}">
                  </div>
                  <div class="form-group row align-items-center">
                    <label for="" class="col-3">Mission Description</label>
                    <input type="text" class="form-control col-9" value="{{ $mission->description }}">
                  </div>
                  <div class="form-group row align-items-start">
                    <label for="" class="col-3">Mission Content</label>
                    <div class="col-9 p-3 border rounded form-control">{!! $mission->content !!}</div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="list-stage" role="tabpanel" aria-labelledby="pills-stage-tab">
            <div class="card">
              <div class="card-header row">
                <div class="col">
                  <h4>Detail Badge</h4>
                </div>
              </div>
              <div class="card-body">
                @if($mission->badge)
                <form>
                  <div class="form-group row align-items-center">
                    <label for="" class="col-3">Badge Title</label>
                    <input type="text" class="form-control col-9" value="{{ $mission->badge->title }}">
                  </div>
                  <div class="form-group row align-items-center">
                    <label for="" class="col-3">Badge Description</label>
                    <input type="text" class="form-control col-9" value="{{ $mission->badge->description }}">
                  </div>
                </form>
                @else
                <div class="row justify-content-center py-5">
                  <div class="col-5 text-center">
                    <a href="" class="btn btn-primary btn-round">Add New
                      Badge</a>
                  </div>
                </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
  function deleteData(url) {
      console.log(url);
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        buttons: {
          confirm: {
            text: 'Yes, delete it!',
            className: 'btn btn-success'
          },
          cancel: {
            visible: true,
            className: 'btn btn-danger'
          }
        }
      }).then((Delete) => {
        if (Delete) {
          swal({
            title: 'Deleted!',
            text: 'Your file has been deleted.',
            type: 'success',
            buttons: {
              confirm: {
                className: 'btn btn-success'
              }
            }
          });
          window.location.href = url;
        } else {
          swal.close();
        }
      });
    }
</script>
@endsection