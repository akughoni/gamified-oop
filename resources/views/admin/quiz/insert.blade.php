@extends("admin.app")

@section('css')
<link rel="stylesheet" href="{{ asset("admin/summernote/summernote-bs4.css") }}">
@endsection

@section("content")
<div class="content">
  <div class="page-inner">
    <div class="page-header">
      <h4 class="page-title">Create New Quiz</h4>
      <ul class="breadcrumbs">
        <li class="nav-home">
          <a href="{{ route("admin.dashboard.index") }}">
            <i class="flaticon-home"></i>
          </a>
        </li>
        <li class="separator">
          <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
          <a href="{{ route("admin.course.index") }}">Data</a>
        </li>
        <li class="separator">
          <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
          <a href="#">Create Data</a>
        </li>
      </ul>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <form action="{{ route("admin.quiz.store") }}" method="POST">
              @csrf
              <input type="hidden" name="course_id" value="{{ $course_id }}">
              <div class="form-group row @error(" question") has-error has-feedback @enderror">
                <label for="question" class="col-2">Quiz Question</label>
                <textarea name="question" class="col-10" id="question" rows="5">{{ old("question") }}</textarea>
                @error("question")
                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group row @error(" answer_a") has-error has-feedback @enderror">
                <label for="cotent" class="col-2">Option A</label>
                <textarea name="answer_a" class="col-10" id="answer_a" rows="5">{{ old("answer_a") }}</textarea>
                @error("answer_a")
                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group row @error(" answer_b") has-error has-feedback @enderror">
                <label for="cotent" class="col-2">Option B</label>
                <textarea name="answer_b" class="col-10" id="answer_b" rows="5">{{ old("answer_b") }}</textarea>
                @error("answer_b")
                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group row @error(" answer_c") has-error has-feedback @enderror">
                <label for="cotent" class="col-2">Option C</label>
                <textarea name="answer_c" class="col-10" id="answer_c" rows="5">{{ old("answer_c") }}</textarea>
                @error("answer_c")
                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group row @error(" answer_d") has-error has-feedback @enderror">
                <label for="cotent" class="col-2">Option D</label>
                <textarea name="answer_d" class="col-10" id="answer_d" rows="5">{{ old("answer_d") }}</textarea>
                @error("answer_d")
                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group row">
                <label for="defaultSelect" class="col-2">Default Select</label>
                <select name="correct_answer" class="form-control form-control col-10" id="defaultSelect">
                  <option>A</option>
                  <option>B</option>
                  <option>C</option>
                  <option>D</option>
                </select>
              </div>
              <div class="form-group">
                <button class="btn btn-primary" type="submit">Sumbit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
@endsection
@section('js')
<script src="{{ asset("admin/summernote/summernote-bs4.js") }}"></script>
<script>
  $(document).ready(function() {
    $('#question').summernote({
      height: 200,
    });
    $('#answer_a').summernote({
      height: 100,
    });
    $('#answer_b').summernote({
      height: 100,
    });
    $('#answer_c').summernote({
      height: 100,
    });
    $('#answer_d').summernote({
      height: 100,
    });
  });
</script>
@endsection