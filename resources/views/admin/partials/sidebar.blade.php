<!-- Sidebar -->
<div class="sidebar sidebar-style-2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <ul class="nav nav-primary">
                <li class="nav-item {{ (request()->is('admin/dashboard')) ? 'active' : '' }}">
                    <a href="{{ route("admin.dashboard.index") }}" class="" aria-expanded="false">
                        <i class="fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item {{ (request()->is('admin/course*')) ? 'active' : '' }}">
                    <a href="{{ route("admin.course.index") }}" class="" aria-expanded="false">
                        <i class="fas fa-boxes"></i>
                        <p>Course</p>
                    </a>
                </li>
                <li class="nav-item {{ (request()->is('admin/mission*')) ? 'active' : '' }}">
                    <a href="{{ route("admin.mission.index") }}" class="" aria-expanded="false">
                        <i class="fas fa-trophy"></i>
                        <p>Mission</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- End Sidebar -->