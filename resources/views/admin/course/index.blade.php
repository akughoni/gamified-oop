@extends("admin.app")

@section("content")
<div class="content">
  <div class="page-inner">
    <div class="page-header">
      <h4 class="page-title">Data Courses</h4>
      <ul class="breadcrumbs">
        <li class="nav-home">
          <a href="{{ route('admin.dashboard.index') }}">
            <i class="flaticon-home"></i>
          </a>
        </li>
        <li class="separator">
          <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
          <a href="#">Tables</a>
        </li>
        <li class="separator">
          <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
          <a href="#">Datatables</a>
        </li>
      </ul>
      <div class="ml-md-auto py-2 py-md-0">
        <a href="{{ route("admin.course.create") }}" class="btn btn-secondary btn-round">Add New Course</a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table id="basic-datatables" class="display table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Level</th>
                    <th>Title</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($courses as $course)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $course->level }}</td>
                    <td>{{ $course->title }}</td>
                    <td class="text-center">
                      <a href="{{ route("admin.course.show",["id" => $course->id]) }}">
                        <button type="button" class="btn btn-sm btn-icon btn-primary">
                          <i class="fas fa-eye"></i>
                        </button>
                      </a>
                      <a href="{{ route("admin.course.edit",["id" => $course->id]) }}">
                        <button type="button" class="btn btn-sm btn-icon  btn-warning">
                          <i class="fas fa-edit"></i>
                        </button>
                      </a>
                      <a href="javascript:deleteData('{{ route('admin.course.delete',['id' => $course->id]) }}')">
                        <button type="button" class="btn btn-sm btn-icon btn-danger">
                          <i class="fas fa-trash-alt"></i>
                        </button>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
@endsection
@section('js')
<script>
  $('#basic-datatables').DataTable({
    });
    function deleteData(url) {
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        buttons: {
          confirm: {
            text: 'Yes, delete it!',
            className: 'btn btn-success'
          },
          cancel: {
            visible: true,
            className: 'btn btn-danger'
          }
        }
      }).then((Delete) => {
        if (Delete) {
          swal({
            title: 'Deleted!',
            text: 'Your file has been deleted.',
            type: 'success',
            buttons: {
              confirm: {
                className: 'btn btn-success'
              }
            }
          });
          window.location.href = url;
        } else {
          swal.close();
        }
      });
    }
</script>
@endsection
