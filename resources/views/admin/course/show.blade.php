@extends("admin.app")

@section("content")
<div class="content">
  <div class="page-inner">
    <div class="page-header">
      <h4 class="page-title">Data Courses</h4>
      <ul class="breadcrumbs">
        <li class="nav-home">
          <a href="{{ route("admin.dashboard.index") }}">
            <i class="flaticon-home"></i>
          </a>
        </li>
        <li class="separator">
          <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
          <a href="{{ route("admin.course.index") }}">Data</a>
        </li>
        <li class="separator">
          <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
          <a href="#">Show Data</a>
        </li>
      </ul>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="col-12 py-2">
            <ul class="nav nav-pills nav-pills-icons nav-secondary nav-pills-no-bd" id="pills-tab-without-border"
              role="tablist">
              <li class="nav-item col-2 text-center">
                <a class="nav-link active" id="pills-course-tab-nobd" data-toggle="pill" href="#detail-course"
                  role="tab" aria-controls="pills-course-nobd" aria-selected="true">
                  <i class="icon-social-dropbox"></i>
                  Course Detail
                </a>
              </li>
              <li class="nav-item col-2 text-center">
                <a class="nav-link" id="pills-stage-tab-nobd" data-toggle="pill" href="#list-stage" role="tab"
                  aria-controls="pills-stage-nobd" aria-selected="false">
                  <i class="icon-directions"></i>
                  Stage
                </a>
              </li>
              <li class="nav-item col-2 text-center">
                <a class="nav-link" id="pills-quiz-tab-nobd" data-toggle="pill" href="#list-quiz" role="tab"
                  aria-controls="pills-quiz-nobd" aria-selected="false">
                  <i class="icon-puzzle"></i>
                  Quiz
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="tab-content mt-2 mb-3" id="show-course">
          <div class="tab-pane fade show active" id="detail-course" role="tabpanel" aria-labelledby="pills-course-tab">
            <div class="card">
              <div class="card-header">
                <h4>Course Detail</h4>
              </div>
              <div class="card-body">
                <form>
                  <div class="form-group row align-items-center">
                    <label for="" class="col-3">Course Title</label>
                    <input type="text" class="form-control col-9" value="{{ $course->title }}">
                  </div>
                  <div class="form-group row align-items-center">
                    <label for="" class="col-3">Course Level</label>
                    <input type="text" class="form-control col-9" value="{{ $course->level }}">
                  </div>
                  <div class="form-group row align-items-start">
                    <label for="" class="col-3">Course Image</label>
                    <img src="{{ asset(Storage::url($course->image)) }}" alt="image" class="img-thumbnail col-4">
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="list-stage" role="tabpanel" aria-labelledby="pills-stage-tab">
            <div class="card">
              <div class="card-header row">
                <div class="col">
                  <h4>List Stage</h4>
                </div>
                <div class="ml-md-auto mr-md-5">
                  <a href="{{ route("admin.stage.create", ["course_id" => $course->id]) }}"
                    class="btn btn-sm btn-success btn-round">Add New
                    Stage</a>
                </div>
              </div>
              <div class="card-body">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Title</th>
                      <th scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($course->stages as $stage)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $stage->title }}</td>
                      <td class="text-center">
                        {{-- <a href="">
                          <button type="button" class="btn btn-sm btn-icon btn-primary">
                            <i class="fas fa-eye"></i>
                          </button>
                        </a> --}}
                        <a href="{{ route("admin.stage.edit", ['id' => $stage->id]) }}">
                          <button type="button" class="btn btn-sm btn-icon  btn-warning">
                            <i class="fas fa-edit"></i>
                          </button>
                        </a>
                        <a href="javascript:deleteData('{{ route("admin.stage.delete", ["id" => $stage->id]) }}')">
                          <button type="button" class="btn btn-sm btn-icon btn-danger">
                            <i class="fas fa-trash-alt"></i>
                          </button>
                        </a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="list-quiz" role="tabpanel" aria-labelledby="pills-quiz-tab">
            <div class="card">
              <div class="card-header row">
                <div class="col">
                  <h4>List Quiz</h4>
                </div>
                <div class="ml-md-auto mr-md-5">
                  <a href="{{ route("admin.quiz.create", ["course_id" => $course->id]) }}"
                    class="btn btn-sm btn-success btn-round">Add New
                    Quiz</a>
                </div>
              </div>
              <div class="card-body">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th width="10%" scope="col">#</th>
                      <th width="70%" scope="col">Question</th>
                      <th scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($course->quizs as $quiz)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $quiz->question }}</td>
                      <td class="text-center">
                        <a href="{{ route("admin.quiz.edit", ['id' => $quiz->id]) }}">
                          <button type="button" class="btn btn-sm btn-icon  btn-warning">
                            <i class="fas fa-edit"></i>
                          </button>
                        </a>
                        <a href="javascript:deleteData('{{ route("admin.quiz.delete", ["id" => $quiz->id]) }}')">
                          <button type="button" class="btn btn-sm btn-icon btn-danger">
                            <i class="fas fa-trash-alt"></i>
                          </button>
                        </a>
                      </td>
                    </tr>
                    @empty
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
  function deleteData(url) {
      console.log(url);
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        buttons: {
          confirm: {
            text: 'Yes, delete it!',
            className: 'btn btn-success'
          },
          cancel: {
            visible: true,
            className: 'btn btn-danger'
          }
        }
      }).then((Delete) => {
        if (Delete) {
          swal({
            title: 'Deleted!',
            text: 'Your file has been deleted.',
            type: 'success',
            buttons: {
              confirm: {
                className: 'btn btn-success'
              }
            }
          });
          window.location.href = url;
        } else {
          swal.close();
        }
      });
    }
</script>
@endsection
