@extends("admin.app")

@section("content")
<div class="content">
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Data Courses</h4>
            <ul class="breadcrumbs">
                <li class="nav-home">
                    <a href="{{ route("admin.dashboard.index") }}">
                        <i class="flaticon-home"></i>
                    </a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="{{ route("admin.course.index") }}">Data</a>
                </li>
                <li class="separator">
                    <i class="flaticon-right-arrow"></i>
                </li>
                <li class="nav-item">
                    <a href="#">Update Data</a>
                </li>
            </ul>
            {{--      <div class="ml-md-auto py-2 py-md-0">--}}
            {{--        <a href="#" class="btn btn-secondary btn-round">Add New Course</a>--}}
            {{--      </div>--}}
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route("admin.course.update",["id" => $course->id]) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="form-group @error(" title") has-error has-feedback @enderror">
                                <label for="title">Course Title</label>
                                <input type="text" class="form-control" name="title"
                                    value="{{ $course->title ?? old("title") }}" id="title" placeholder="Title">
                                @error("title")
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group @error(" level") has-error has-feedback @enderror">
                                <label for="level">Level</label>
                                <input type="text" class="form-control" name="level"
                                    value="{{ $course->level ?? old("level") }}" id="level" placeholder="level">
                                @error("level")
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group @error(" image") has-error has-feedback @enderror">
                                <label for="image">Course Icon</label>
                                <input type="file" class="form-control-file" id="image" name="image">
                                @error("image")
                                <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <img src="{{ asset(Storage::url($course->image)) }}" alt="image"
                                    class="img-thumbnail col-3">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Sumbit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@section('js')
<script>

</script>
@endsection