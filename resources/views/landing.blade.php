<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>GamOOP - Easy Way To Learn OOP</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Krub:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

    <!-- =======================================================
    * Template Name: Bikin - v2.1.0
    * Template URL: https://bootstrapmade.com/bikin-free-simple-landing-page-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

        <h1 class="logo mr-auto text-uppercase"><a href="index.html">Gamified OOP</a></h1>
        <a href="{{ route("user.login") }}" class="get-started-btn scrollto">Coba Sekarang</a>

    </div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">

    <div class="container d-flex flex-column align-items-center justify-content-center" data-aos="fade-up">
        <h1>Belajar OOP Kapan Saja. Tanpa Bosan.</h1>
        <h2>Ayo Mulai Belajar OOP Dengan Cara Baru Yang Menyenangkan.</h2>
        <a href="{{ route("user.dashboard.index") }}" class="btn-get-started scrollto">Mulai Sekarang</a>
        <img src="{{ asset('assets/img/landing-page.png') }}" class="img-fluid hero-img" alt="" data-aos="zoom-in" data-aos-delay="150">
    </div>

</section><!-- End Hero -->

<main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
        <div class="container">

            <div class="row no-gutters">
                <div class="content col-xl-4 d-flex align-items-stretch" data-aos="fade-right">
                    <div class="content">
                        <img src="{{ asset('assets/img/about-img.png') }}" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="col-xl-8 d-flex align-items-stretch" data-aos="fade-left">
                    <div class="icon-boxes d-flex flex-column justify-content-center">
                        <div class="content">
                            <h3>Cara baru terbaik untuk belajar OOP.</h3>
                            <p>
                                Belajar dengan Gamified OOP itu mudah, menyenangkan, dan bikin ketagihan. Kamu bisa medapatkan poin untuk setiap materi yang kamu selesaikan, asah pegetahuanmu dengan menyelesaikan semua quiz, berpaculah dengan kawan-kawanmu, dan tingkatkan keahlianmu menulis kode program dengan menjalankan semua misi yang tersedia. Pelajaran-pelajaran singkat kami efektif, dan sudah banyak bukti bahwa metode ini berhasil.
                            </p>
                        </div>
                    </div><!-- End .content-->
                </div>
            </div>

        </div>
    </section><!-- End About Section -->

    <!-- ======= Features Section ======= -->
    <section id="features" class="features" data-aos="fade-up">
        <div class="container">

            <div class="section-title">
                <h2>Belajar dengan penuh keseruan seperti saat kamu bermain.</h2>
            </div>

            <div class="row content">
                <div class="col-md-5" data-aos="fade-right" data-aos-delay="100">
                    <img src="{{ asset('assets/img/features-1.png') }}" class="img-fluid" alt="">
                </div>
                <div class="col-md-7 pt-4" data-aos="fade-left" data-aos-delay="100">
                    <h3>Pembelajaran sesuai Kurikulum .</h3>
                    <p class="font-italic">
                        Pembelajaran di Gamified OOP teah dirancang sesuai dengan Kurikulum 2013 revisi 2018 untuk SMK yang saat ini berlaku.
                    </p>
                    <ul>
                        <li><i class="icofont-check"></i> Sebaran materi sesuai Kompetensi Keahlian RPL.</li>
                        <li><i class="icofont-check"></i> Tujuan Pembelajaran yang sistematis.</li>
                        <li><i class="icofont-check"></i> Tes singkat untuk mengukur pemahaman siswa.</li>
                    </ul>
                </div>
            </div>

            <div class="row content">
                <div class="col-md-5 order-1 order-md-2" data-aos="fade-left">
                    <img src="{{ asset('assets/img/features-2.png') }}" class="img-fluid" alt="">
                </div>
                <div class="col-md-7 pt-5 order-2 order-md-1" data-aos="fade-right">
                    <h3>Materi yang mudah dipelajari</h3>
                    <p class="font-italic">
                        Tidak seperti buku teks yang cenderung membosankan. Materi PBO pada Gamified OOP dikemas dengan menarik.
                    </p>
                    <p>
                        Materi PBO yang kamu pelajari dalam satu semester menjadi 9 level yang masing-masing terdiri dari stage-stage kecil. Pembagian level dan stage akan memudahkan belajarmu dan menuntunmu untuk berada di jalur yang benar dalam belajar.
                    </p>
                </div>
            </div>

            <div class="row content">
                <div class="col-md-5" data-aos="fade-right">
                    <img src="{{ asset('assets/img/features-3.png') }}" class="img-fluid" alt="">
                </div>
                <div class="col-md-7 pt-5" data-aos="fade-left">
                    <h3>Terus termotivasi menjadi yang terbaik</h3>
                    <p>Di Gamified OOP, kami berikan poin penghargaan di setiap usaha yang kamu lakukan.</p>
                    <ul>
                        <li><i class="icofont-check"></i> Menyelesaikan pelajaran +poin.</li>
                        <li><i class="icofont-check"></i> Menyelesaikan kuis +poin.</li>
                        <li><i class="icofont-check"></i> Menyelesaikan misi +poin.</li>
                    </ul>
                </div>
            </div>

            <div class="row content">
                <div class="col-md-5 order-1 order-md-2" data-aos="fade-left">
                    <img src="{{ asset('assets/img/features-4.png') }}" class="img-fluid" alt="">
                </div>
                <div class="col-md-7 pt-5 order-2 order-md-1" data-aos="fade-right">
                    <h3>Tingkatkan kemampuan untuk menyelesaikan misi</h3>
                    <p class="font-italic">
                        Tidak hanya teori konseptual, Gamified OOP juga mendukungmu untuk mengasah kemampuan praktik.
                    </p>
                    <p>
                        Gamified OOP menantangmu untuk menyelesaikan 6 misi yang tersedia. Asah kemampuan menulis kode programmu dengan menyelesaikan semua misi dan kumpulkan lencana. Hanya yang terbaik yang akan menyelesaikan misi.
                    </p>
                </div>
            </div>

        </div>
    </section><!-- End Features Section -->

</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer">
    <!-- Footer-->
    <div class="container d-md-flex py-4">
        <div class="mr-md-auto text-center text-md-left">
            <div class="copyright">
                &copy; Copyright <strong><span>GamOOP</span></strong> 2020.
            </div>
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
<div id="preloader"></div>

<!-- Vendor JS Files -->
<script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
<script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>
<script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/vendor/venobox/venobox.min.js') }}"></script>
<script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{ asset('assets/js/main.js') }}"></script>

</body>

</html>
