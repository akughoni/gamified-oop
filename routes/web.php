<?php

use Illuminate\Support\Facades\Route;

Route::view("/test", 'test' );
Route::view('/sertifikat', 'user.sertifikat');
    Route::view('/', 'landing')->middleware("guest");
Route::group(['namespace' => "User\Auth", "middleware" => "guest", "as" => "user."], function () {
    Route::get("/login", "AuthController@showLogin")->name("login");
    Route::post("/login", "AuthController@login")->name("login.proses");
    Route::get("/register", "AuthController@showRegister")->name("register");
    Route::post("/register", "AuthController@register")->name("register.proses");
});

Route::group(['namespace' => "User", "middleware" => "auth:web", "as" => "user."], function () {
    Route::get("/logout", "Auth\AuthController@logout")->name("logout");
    Route::get("/dashboard", "DashboardController@index")->name("dashboard.index");
    /**
     * Route User Course
     */
    Route::get("/course", "CourseController@index")->name("course.index");
    Route::get("/course/{courseId}", "CourseController@show")->name("course.show");
    Route::get("/course/{courseId}/done", "CourseController@updateCourseIsDone")->name("course.done");

    /**
     *  Route for Quiz
     */
    Route::get("/course/{courseId}/quiz/", "QuizController@index")->name("quiz.index");
    Route::post("/course/{courseId}/quiz/submit", "QuizController@submit")->name("quiz.submit");

    /**
     *  Route for Mission
     */

    Route::get("/mission", "MissionController@index")->name("mission.index");
    Route::get("/mission/{id}", "MissionController@show")->name("mission.show");
    Route::post("/mission/success", "MissionController@takeMission")->name("mission.take");

    Route::get("/leaderboard", "LeaderboardController@index")->name("leaderboard.index");
    Route::get("/certificate", "DashboardController@certificate")->name('certificate');
});




/**
 * This is route for admin page
 */
Route::redirect('admin', 'admin/dashboard');
Route::prefix('admin')->group(function () {
    Route::group(['namespace' => "Admin\Auth", "middleware" => "guest:admin", "as" => "admin."], function () {
        Route::get("/login", "AuthController@showLogin")->name("login");
        Route::post("/login", "AuthController@login")->name("login.proses");
        Route::get("/register", "AuthController@showRegister")->name("register");
        Route::post("/register", "AuthController@register")->name("register.proses");
    });

    Route::group(['namespace' => "Admin", "middleware" => "auth:admin", "as" => "admin."], function () {
        Route::get("/logout", "Auth\AuthController@logout")->name("logout");
        Route::get("/dashboard", "DashboardController@index")->name("dashboard.index");

        /**
         * Route Courses Page
         */
        Route::get("/course", "CourseController@index")->name("course.index");
        Route::get("/course/create", "CourseController@create")->name("course.create");
        Route::post("/course/store", "CourseController@store")->name("course.store");
        Route::get("/course/{id}/show", "CourseController@show")->name("course.show");
        Route::get("/course/{id}/edit", "CourseController@edit")->name("course.edit");
        Route::post("/course/{id}/update", "CourseController@update")->name("course.update");
        Route::get("/course/{id}/delete", "CourseController@delete")->name("course.delete");

        /**
         * Route for Stage
         */
        Route::get("/course/stage/create/{course_id}", "StageController@create")->name("stage.create");
        Route::post("/course/stage/store", "StageController@store")->name("stage.store");
        Route::get("/course/stage/{id}/edit", "StageController@edit")->name("stage.edit");
        Route::post("/course/stage/{id}/update", "StageController@update")->name("stage.update");
        Route::get("/course/stage/{id}/delete", "StageController@delete")->name("stage.delete");

        /**
         * Route for Quiz
         */
        Route::get("/course/quiz/create/{course_id}", "QuizController@create")->name("quiz.create");
        Route::post("/course/quiz/store", "QuizController@store")->name("quiz.store");
        Route::get("/course/quiz/{id}/edit", "QuizController@edit")->name("quiz.edit");
        Route::post("/course/quiz/{id}/update", "QuizController@update")->name("quiz.update");
        Route::get("/course/quiz/{id}/delete", "QuizController@delete")->name("quiz.delete");

        /**
         * Route for Mission
         */
        Route::get("/mission", "MissionController@index")->name("mission.index");
        Route::get("/mission/create", "MissionController@create")->name("mission.create");
        Route::post("/mission/store", "MissionController@store")->name("mission.store");
        Route::get("/mission/{id}/edit", "MissionController@edit")->name("mission.edit");
        Route::post("/mission/{id}/update", "MissionController@update")->name("mission.update");
        Route::get("/mission/{id}/delete", "MissionController@delete")->name("mission.delete");
        Route::get("/mission/{id}/show", "MissionController@show")->name("mission.show");
    });
});

/**
 *  Route for image upload
 */

