<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CourseTableSeeder::class);
        $this->call(MissionTableSeeder::class);
        $this->call(BadgeTableSeeder::class);
        $this->call(QuizTableSeeder::class);
        $this->call(StageTableSeeder::class);
    }
}
