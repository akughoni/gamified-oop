<?php

use App\Models\Level;
use App\Models\User;
use App\Models\TakeCourse;
use App\Models\UserBadge;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 5)->create();
    }
}
