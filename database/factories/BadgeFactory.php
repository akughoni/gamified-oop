<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Badge;
use Faker\Generator as Faker;

$factory->define(Badge::class, function (Faker $faker) {
    static $mission_id = 1;
    return [
        "title"    => $faker->sentence(1),
        "description" => $faker->sentence(3),
        "mission_id"  => $mission_id++
    ];
});
