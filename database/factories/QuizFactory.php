<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Quiz;
use Faker\Generator as Faker;

$factory->define(Quiz::class, function (Faker $faker) {
    return [
        "course_id"   => rand(1, 5),
        "question"    => $faker->paragraph(1),
        "answer_a"    => $faker->sentence(rand(2, 4)),
        "answer_b"    => $faker->sentence(rand(2, 4)),
        "answer_c"    => $faker->sentence(rand(2, 4)),
        "answer_d"    => $faker->sentence(rand(2, 4)),
        "correct_answer"    => "a",
    ];
});
