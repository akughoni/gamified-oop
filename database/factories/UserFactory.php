<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $class = ["XI-RPL-A","XI-RPL-B","XI-RPL-C","XI-RPL-D"];
    return [
        "fullname"     => $faker->name,
        "username"     => $faker->userName,
        "phone"        => $faker->e164PhoneNumber,
        "class"        => $class[rand(0, 3)],
        "password"     => bcrypt("asdasd"),
        "image"        => "public/boy-ava.png",
    ];
});
