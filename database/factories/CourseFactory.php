<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    static $level = 1;
    return [
        "level"   => $level++,
        "title"   => $faker->sentence(2),
        "image"   => "public/courses/konsep.png",
    ];
});
