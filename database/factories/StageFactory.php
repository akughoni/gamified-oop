<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Stage;
use Faker\Generator as Faker;

$factory->define(Stage::class, function (Faker $faker) {
    return [
        "course_id"   => rand(1, 5),
        "title"       => $faker->sentence(3),
        "content"     => $faker->paragraph(4),
        "video"       => "https://www.youtube.com/watch?v=Xi6tZcDAHgU"
    ];
});
