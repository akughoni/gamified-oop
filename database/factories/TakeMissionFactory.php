<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TakeMission;
use Faker\Generator as Faker;

$factory->define(TakeMission::class, function (Faker $faker) {
    return [
        "user_id"    => rand(1, 5),
        "mission_id" => rand(1, 5)
    ];
});
