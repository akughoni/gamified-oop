<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TakeCourse;
use Faker\Generator as Faker;

$factory->define(TakeCourse::class, function (Faker $faker) {
    return [
        "user_id"    => rand(1, 5),
        "course_id"  => rand(1, 5)
    ];
});
