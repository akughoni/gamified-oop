<?php

namespace App\Providers;

use App\Repositories\BaseContract;
use App\Repositories\BaseRepository;
use App\Repositories\Course\CourseContract;
use App\Repositories\Course\CourseContruct;
use App\Repositories\Course\CourseRepository;
use App\Repositories\Mission\MissionContract;
use App\Repositories\Mission\MissionRepository;
use App\Repositories\Quiz\QuizContract;
use App\Repositories\Quiz\QuizRepository;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    protected $repositories = [
        BaseContract::class   => BaseRepository::class,
        CourseContract::class => CourseRepository::class,
        QuizContract::class   => QuizRepository::class,
        MissionContract::class => MissionRepository::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->repositories as $contract => $repository) {
            $this->app->bind($contract, $repository);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
