<?php

namespace App\Http\Requests\User;

use App\Traits\NotifyMessage;
use Illuminate\Foundation\Http\FormRequest;

class RegisterStoreRequest extends FormRequest
{
    use NotifyMessage;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "fullname"     => "required",
            "username"     => "required",
            "phone"     => "required",
            "class"     => "required",
            "password"     => "required|min:5|confirmed",
        ];
    }
    public function messages()
    {
        $this->setNotifyMessage("failed", "Data yang dimasukkan kurang benar");
        return [
            "fullname.required" => "Fullname can not be empty",
            "username.required" => "Fullname can not be empty",
            "phone.required" => "Fullname can not be empty",
            "class.required" => "Fullname can not be empty",
            "password.required" => "Fullname can not be empty",
        ];
    }
}
