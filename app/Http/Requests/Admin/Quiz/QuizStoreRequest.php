<?php

namespace App\Http\Requests\Admin\Quiz;

use Illuminate\Foundation\Http\FormRequest;

class QuizStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "course_id"        => "required",
            "question"         => "required",
            "answer_a"         => "required",
            "answer_b"         => "required",
            "answer_c"         => "required",
            "answer_d"         => "required",
            "correct_answer"   => "required"
        ];
    }
}
