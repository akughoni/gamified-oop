<?php

namespace App\Http\Requests\Admin\Mission;

use Illuminate\Foundation\Http\FormRequest;

class MissionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title"         => "required",
            "descripton"    => "required",
            "content"       => "required",
            "badge_title"   => "required",
            "badge_description" => "required"
        ];
    }

    public function messages()
    {
        return [
            "title.required"      => "Harus di isi",
            "descripton.required"      => "Harus di isi",
            "content.required"      => "Harus di isi",
            "badge_title.required"      => "Harus di isi",
            "badge_description.required"      => "Harus di isi",
        ];
    }
}
