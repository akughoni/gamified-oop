<?php

namespace App\Http\Controllers\Admin;

use App\Traits\NotifyMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class BaseAdminController extends Controller
{
    use NotifyMessage;

    public function uploadImage($request, $path, $inputName)
    {
        $path = Storage::putFile($path, $request->file($inputName));
        return $path;
    }

    public function deleteImage($path)
    {
        return Storage::delete($path);
    }
}
