<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Quiz\QuizStoreRequest;
use App\Models\Quiz;
use Illuminate\Http\Request;

class QuizController extends BaseAdminController
{
    public function create($course_id)
    {
        return view("admin.quiz.insert", compact('course_id'));
    }

    public function edit($id)
    {
        $quiz = Quiz::find($id);
        return view("admin.quiz.update", compact('quiz'));
    }

    public function store(QuizStoreRequest $request)
    {
        $data = $request->validated();
        $quizStore = Quiz::create($data);
        if (!$quizStore) {
            $this->setNotifyMessage("failed", "Create data failed!");
            return redirect()->back()->withInput();
        }
        $this->setNotifyMessage("success", "Create data success!");
        return redirect()->route("admin.course.show", ["id" => $request->course_id]);
    }

    public function update(QuizStoreRequest $request, $id)
    {
        $data = $request->validated();
        $quiz = Quiz::find($id)->update($data);
        if (!$quiz) {
            $this->setNotifyMessage("failed", "Update data failed!");
            return redirect()->back()->withInput();
        }
        $this->setNotifyMessage("success", "Update data success!");
        return redirect()->route("admin.course.show", ["id" => $request->course_id]);
    }

    public function delete($id)
    {
        $quiz = Quiz::find($id);
        $course_id = $quiz->course_id;
        $quiz->delete();
        if (!$quiz) {
            $this->setNotifyMessage("failed", "Delete data failed!");
            return redirect()->route("admin.course.show", ["id" => $course_id]);
        }
        $this->setNotifyMessage("success", "Delete data success!");
        return redirect()->route("admin.course.show", ["id" => $course_id]);
    }
}
