<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ImageController extends Controller
{
    public function upload_image(Request $request)
    {
        $file = $request->file('file');
        $tujuan_upload = 'admin-upload';
        $image_name= time().".".$file->getClientOriginalExtension();
        $fullpath= "/admin-upload/". $image_name;
        $file->move($tujuan_upload,$image_name);
        echo $fullpath;
    }

    //Delete image summernote
    public function delete_image(Request $request){
        $src = $request->src;
        $file_name = str_replace(url('/'),'', $src);
        $file_path = public_path($file_name);
        if(File::exists($file_path)) {
            return File::delete($file_path);
        }else{
            return "Delete failed";
        }
    }
}
