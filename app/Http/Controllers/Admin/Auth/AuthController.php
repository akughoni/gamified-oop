<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Traits\NotifyMessage;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use NotifyMessage;

    public function showLogin()
    {
        return view("admin.auth.login");
    }

    public function login(Request $request)
    {
        $request->validate([
            "email" => "required",
            "password" => "required"
        ]);
        $data = $request->only("email", "password");
        if (Auth::guard("admin")->attempt($data)) {
            $this->setNotifyMessage("success", "Welcome to dashboard");
            return redirect()->intended("/admin/dashboard");
        }
        $this->setNotifyMessage("failed", "Email or password is wrong!");
        return redirect()->back()->withInput();
    }

    public function showRegister()
    {
        return view("admin.auth.register");
    }

    public function register(Request $request)
    {
        $request->validate([
            "email" => "required",
            "name" => "required",
            "password" => "required"
        ]);

        User::create([
           "email" => $request->email,
           "name" => $request->name,
           "password" => Hash::make($request->password)
        ]);

        $this->setNotifyMessage("success", "Create user account success!");
        return redirect()->route("login");
    }

    public function logout()
    {
        Auth::logout();
        $this->setNotifyMessage("success", "You are logout!");
        return redirect()->route("admin.login");
    }
}
