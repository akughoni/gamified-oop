<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Stage\StageStoreRequest;
use App\Http\Requests\Admin\Stage\StageUpdateRequest;
use App\Models\Stage;
use Illuminate\Http\Request;

class StageController extends BaseAdminController
{
    public function create($course_id)
    {
        return view("admin.stage.insert", compact('course_id'));
    }

    public function store(StageStoreRequest $request)
    {
        $data = Stage::create($data->except("files"));
        $this->setNotifyMessage("success", "Crate data success!");
        return redirect()->route("admin.course.show", ['id' => $request->course_id]);
    }

    public function edit($id)
    {
        $stage = Stage::find($id);
        return view("admin.stage.update", compact('stage'));
    }

    public function update(StageUpdateRequest $request, $id)
    {
        $stage = Stage::find($id)->update($request->except('files'));
        $this->setNotifyMessage("success", "Update data success!");
        return redirect()->route("admin.course.show", ['id' => $request->course_id]);
    }

    public function delete($id)
    {
        $stage = Stage::find($id)->delete();
        $this->setNotifyMessage("success", "Delete data success!");
        return redirect()->back();
    }
}
