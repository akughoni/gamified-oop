<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Mission\MissionStoreRequest;
use App\Models\Badge;
use App\Models\Mission;
use Illuminate\Http\Request;

class MissionController extends BaseAdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $missions = Mission::all();
        return view("admin.mission.index", compact('missions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.mission.insert");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attribute = $request->except(["files", "badge_title","badge_description"]);
        $data = Mission::create($attribute);
        $this->createMissionBadge($request, $data);
        if (!$data) {
            $this->setNotifyMessage("failed", "Crate data failed!");
            return redirect()->back()->withInput();
        }
        $this->setNotifyMessage("success", "Crate data success!");
        return redirect()->route("admin.mission.index");
    }

    public function createMissionBadge($request, $mission)
    {
        $badge = new Badge();
        $badge->mission_id = $mission->id;
        $badge->title = $request->badge_title;
        $badge->description = $request->badge_description;
        $badge->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mission = Mission::with("badge")->find($id);
        return view("admin.mission.show", compact("mission"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mission = Mission::with("badge")->find($id);
        return view("admin.mission.update", compact("mission"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mission = Mission::find($id);
        $mission->update($request->except(["files", "badge_title","badge_description"]));
        $mission->badge->update([
            "title" => $request->badge_title,
            "description" => $request->badge_description,
        ]);
        $this->setNotifyMessage("success", "Update data success!");
        return redirect()->route("admin.mission.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $mission = Mission::find($id);
        $mission->badge->delete();
        $mission->delete();
        $this->setNotifyMessage("success", "Delete data success!");
        return redirect()->back();
    }
}
