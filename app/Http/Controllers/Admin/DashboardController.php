<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\BaseAdminController;

class DashboardController extends BaseAdminController
{
    public function index()
    {
        $siswa = User::with('level')->get();
        return view("admin.dashboard.index", compact('siswa'));
    }
}
