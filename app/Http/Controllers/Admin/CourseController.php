<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseAdminController;
use App\Http\Requests\Admin\Course\CourseStoreRequest;
use App\Models\Course;
use App\Models\Stage;
use Illuminate\Http\Request;

class CourseController extends BaseAdminController
{
    public function index()
    {
        $courses = Course::all();
        return view("admin.course.index", compact('courses'));
    }

    public function show($id)
    {
        $course = Course::with("stages")->get()->find($id);
        return view("admin.course.show", compact("course"));
    }

    public function create()
    {
        return view("admin.course.insert");
    }

    public function store(CourseStoreRequest $request)
    {
        $attribute = $request->all();
        $pathUrl = $this->uploadImage($request, "public/course", "image");
        $attribute["image"] = $pathUrl;
        $data = Course::create($attribute);
        if (!$data) {
            $this->setNotifyMessage("failed", "Crate data failed!");
            return redirect()->back()->withInput();
        }
        $this->setNotifyMessage("success", "Crate data success!");
        return redirect()->route("admin.course.index");
    }

    public function edit($id)
    {
        $course = Course::find($id);
        return view("admin.course.update", compact("course"));
    }

    public function update(Request $request, $id)
    {
        $data = Course::find($id);
        $attribute = $request->all();
        if ($request->has("image")) {
            $delete = $this->deleteImage($data->image);
//            dd($delete);
            $pathUrl = $this->uploadImage($request, "public/course", "image");
            $attribute["image"] = $pathUrl;
        }
        $data->update($attribute);
        $this->setNotifyMessage("success", "Update data success!");
        return redirect()->route("admin.course.index");
    }

    public function delete($id)
    {
        $data = Course::find($id);
        $this->deleteImage($data->image);
        $data->delete();
        $this->setNotifyMessage("success", "Delete data success!");
        return redirect()->route("admin.course.index");
    }
}
