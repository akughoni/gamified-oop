<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class LeaderboardController extends BaseUserController
{
    public function index()
    {
        $users = User::with("points")->get();
        $users = $users->sortByDesc(function ($item) {
            return $item->points()->sum("point");
        })->take(10);

        return view("user.leaderboard.index", compact("users"));
    }
}
