<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Quiz;
use App\Repositories\Course\CourseRepository;
use App\Repositories\Quiz\QuizRepository;
use Illuminate\Http\Request;

class QuizController extends BaseUserController
{
    public $minimal_score = 70;
    public $quizRepository;

    public function __construct(QuizRepository $quizRepository)
    {
        $this->quizRepository = $quizRepository;
    }

    public function index(Request $request, $courseId)
    {
        $page = $request->page;
        $quiz = $this->quizRepository->getQuizPaginate($courseId);
        $count = $this->quizRepository->amoutQuestion($courseId);
        if ($request->ajax()) {
            return view("user.quiz.question", compact("quiz", "page", "count", "courseId"))->render();
        }
        return view("user.quiz.index", compact("quiz", "page", "count", "courseId"));
    }

    public function submit(Request $request, $courseId)
    {
        $score = $this->quizRepository->getScore($request);
        if ($score >= $this->minimal_score) {
            $this->quizRepository->setNextCourse(new CourseRepository());
            $this->quizRepository->setUserPoint(auth()->user()->id, "quiz", $courseId, $score);
            $message = "Selamat anda berhasil lulus pada quiz ini, silahkan lanjut course berikutnya";
            return view("user.quiz.report", compact("score", "message"));
        }
        $message = "Sayang sekali, anda belum berhasil pada quiz ini, silahkan pelajari lagi materinya";
        return view("user.quiz.report", compact("score", "message"));
    }
}
