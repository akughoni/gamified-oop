<?php

namespace App\Http\Controllers\User;

use App\Models\Course;
use App\Models\Stage;
use App\Models\TakeCourse;
use App\Models\User;
use App\Repositories\Course\CourseRepository;
use Illuminate\Http\Request;

class CourseController extends BaseUserController
{
    protected $courseRepository;

    public function __construct(CourseRepository $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    public function index()
    {
        $user = $this->courseRepository->userCourse();
        $courseNotTaken = $this->courseRepository->courseNotTaken($user);
        return view("user.course.index", compact("user", "courseNotTaken"));
    }

    public function show($courseId)
    {
        $stage = Stage::where("course_id", $courseId);
        $total = $stage->count();
        $stage = $stage->simplePaginate(1);
        return view("user.course.show", compact("stage", "total"));
    }

    public function updateCourseIsDone($courseId)
    {
        $user = auth()->user();
        $this->courseRepository->updateTakeCourseIsDone($user->id, $courseId);
        $this->courseRepository->setUserPoint($user->id, "course", $courseId, $this->courseRepository->pointFromCourse($courseId));
        return redirect()->route("user.course.index");
    }
}
