<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Models\Level;
use App\Models\User;
use App\Models\Course;
use App\Models\Mission;
use App\Models\TakeCourse;
use App\Models\TakeMission;
use App\Models\UserPoint;

class BaseUserController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = auth()->user();
    }


    public function setUserPoint($userId, $from, $fromId, $point)
    {
        return UserPoint::updateOrCreate([
            "user_id"  => $userId,
            "from"     => $from,
            "from_id"  => $fromId,
        ], [
            "point"    => $point
        ]);
    }

    public function createDataLevel($user)
    {
        $attribute["user_id"] = $user["id"];
        $level = Level::create($attribute);
        return $level;
    }

    public function createDataTakeCourse($user, $course_id = null)
    {
        $attribute["user_id"] = $user["id"];
        $attribute["course_id"] = $course_id ?? $this->getCourseByLevel(1)->id;
        return TakeCourse::firstOrCreate($attribute);
    }

    public function createDataTakeMission($user, $mission_id = null)
    {
        $attribute["user_id"] = $user["id"];
        $attribute["mission_id"] = $mission_id ?? 1;
        return TakeMission::firstOrCreate($attribute);
    }

    public function getCourseByLevel($level)
    {
        return Course::where("level", $level)->first();
    }
}
