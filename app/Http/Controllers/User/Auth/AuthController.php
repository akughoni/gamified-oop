<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\User\BaseUserController;
use App\Http\Requests\User\RegisterStoreRequest;
use App\Traits\NotifyMessage;
use Illuminate\Http\Request;
use App\Models\User;
use App\Repositories\BaseRepository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends BaseUserController
{
    use NotifyMessage, AuthenticatesUsers;

    public function username()
    {
        return "username";
    }

    public function showLogin()
    {
        return view("user.auth.login");
    }

    public function login(Request $request)
    {
        $request->validate([
            "username" => "required",
            "password" => "required"
        ]);
        $data = $request->only("username", "password");
        if (Auth::attempt($data)) {
            $this->setNotifyMessage("success", "Welcome to dashboard");
            return redirect()->intended(route("user.dashboard.index"));
        }
        $this->setNotifyMessage("failed", "User or password is wrong!");
        return redirect()->back()->withInput();
    }

    public function showRegister()
    {
        return view("user.auth.register");
    }

    public function register(RegisterStoreRequest $request, BaseRepository $baseRepository)
    {
        $attribute = $request->except("password_confirmation");
        $attribute["image"] = "public/boy-ava.png";
        $attribute["password"] = Hash::make($attribute["password"]);
        $user = User::create($attribute);
        $baseRepository->createDataInit($user);
        $this->setNotifyMessage("success", "Create user account success!");
        return redirect()->route("user.login");
    }

    public function logout()
    {
        Auth::logout();
        $this->setNotifyMessage("success", "You are logout!");
        return redirect()->route("user.login");
    }
}
