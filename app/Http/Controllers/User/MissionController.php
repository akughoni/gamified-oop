<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Mission;
use App\Models\TakeMission;
use App\Models\User;
use App\Models\UserPoint;
use App\Repositories\Mission\MissionContract;
use Illuminate\Http\Request;

class MissionController extends BaseUserController
{
    public $missionRepo;

    public function __construct(MissionContract $missionRepo)
    {
        $this->missionRepo = $missionRepo;
    }

    public function index()
    {
        $user = $this->missionRepo->userMission();
        $missionsNotTaken = $this->missionRepo->missionNotTaken($user);
        $point = $this->missionRepo->userPoint($user->id);
        return view("user.mission.index", compact("missionsNotTaken", "user", "point"));
    }

    public function show($id =  null)
    {
        return view("user.mission.show")->with('mission', $this->missionRepo->missionById($id));
    }

    public function takeMission(Request $request)
    {
        $data = $request->validate([
            "user_id" => "required",
            "mission_id"  => "required"
        ]);
        $this->missionRepo->updateTakeMissionIsDone($data);
        $this->missionRepo->setUserPoint($data["user_id"], "mission", $data["mission_id"], 10);
    }
}
