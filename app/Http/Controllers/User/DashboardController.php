<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Badge;
use App\Models\Course;
use App\Models\Mission;
use App\Models\User;
use App\Models\UserPoint;
use Illuminate\Http\Request;
use PDF;
use MPDF;

class DashboardController extends Controller
{
    public function index()
    {
        $id = auth()->user()->id;
        $user = User::with("badges")->find($id);
        $badges = $user->badges()->where("take_missions.is_done", 1)->get();
        $point = UserPoint::where("user_id", $id)->get()->sum("point");
        $bagdeId = $badges->map(function ($badge) {
            return $badge->id;
        });
        $badgesNotTaken = Badge::whereNotIn("id", $bagdeId)->get();
        $countCourseIsDone = $user->takeCourse->where('is_done', 1)->count();
        $courseCount = Course::all()->count();

        return view('user.dashboard.index', compact("badgesNotTaken", "user", "point", "badges", 'countCourseIsDone', 'courseCount'));
    }

    public function certificate()
    {
        $data = [
            'link_css' => asset('user/css/styles.css'),
            'link_gambar' => asset('user/assets/img/trophy.png'),
        ];
        return view('user.sertifikat', $data);
    }
}
