<?php
namespace App\Traits;

trait NotifyMessage
{
    /**
     * @param string $key = success or failed
     * @param string $message
     */
    protected function setNotifyMessage(string $key, string $message)
    {
        session()->flash($key,$message);
    }
}


