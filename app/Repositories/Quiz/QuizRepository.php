<?php

namespace App\Repositories\Quiz;

use App\Models\Course;
use App\Models\Level;
use App\Models\Quiz;
use App\Models\User;
use App\Models\UserPoint;
use App\Repositories\BaseRepository;
use App\Repositories\Course\CourseRepository;

class QuizRepository extends BaseRepository implements QuizContract
{
    public function getQuizPaginate($courseId, $limit = 1)
    {
        return Quiz::Question($courseId)->paginate(1);
    }

    public function amoutQuestion($courseId)
    {
        return Quiz::Question($courseId)->count();
    }

    public function checkAnswer($req)
    {
        $score = collect();
        foreach ($req->data as $index => $item) {
            $question = Quiz::where('id', $item['id'])->first();
            if (strtolower($question->correct_answer) == $item['value']) {
                $result = \collect([
                    'id' => $question->id,
                    'status' => 'benar'
                ]);
            } else {
                $result = \collect([
                    'id' => $question->id,
                    'status' => 'salah'
                ]);
            }
            $score->push($result);
        }
        return $score;
    }

    public function countResult($score)
    {
        $true_answer = $score->filter(function ($item) {
            return $item["status"] == "benar";
        });
        $nilai = ($true_answer->count() / $score->count()) * 100;
        return round($nilai, 1);
    }

    public function getScore($req)
    {
        $score = $this->checkAnswer($req);
        return $this->countResult($score);
    }

    public function setNextCourse(CourseRepository $courseRepository)
    {
        $user = auth()->user();
        $user = User::with("takeCourse")->with("coursesTaken")->get()->find(auth()->user()->id);
        if ($this->checkCoursePoint($user)) {
            $courseIdNow = $user->takeCourse->last()->course_id;
            $point = $courseRepository->pointFromCourse($courseIdNow);
            $courseRepository->updateTakeCourseIsDone($user->id, $courseIdNow);
            $this->setUserPoint($user->id, "course", $courseIdNow, $point);
            $user = User::with("takeCourse")->with("coursesTaken")->get()->find(auth()->user()->id);
        }
        $courseTakenId = $courseRepository->courseTakenIdIsDone($user);
        $nextCourse = Course::whereNotIn("id", $courseTakenId)->orderBy("level", "ASC")->first();
        $this->updateUserLevel($user, $nextCourse);
        $this->createDataTakeCourse($user, $nextCourse->id);
    }

    public function updateUserLevel($user, $nextCourse)
    {
        return Level::where("user_id", $user->id)->update([
            "level" => $nextCourse->level
        ]);
    }

    public function checkCoursePoint($user)
    {
        $userId = $user->id;
        $courseId = $user->takeCourse->last()->course_id;
        $coursePoint = UserPoint::where([
            "user_id" => $userId,
            "from" => "course",
            "from_id" => $courseId
        ])->get();
        return $coursePoint->isEmpty();
    }
}
