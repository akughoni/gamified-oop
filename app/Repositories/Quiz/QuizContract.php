<?php

namespace App\Repositories\Quiz;

use App\Repositories\Course\CourseRepository;

interface QuizContract
{
    public function getQuizPaginate($courseId, $limit = 1);

    public function amoutQuestion($courseId);

    public function checkAnswer($req);

    public function countResult($score);

    public function getScore($req);

    public function setNextCourse(CourseRepository $courseRepository);

    public function updateUserLevel($user, $nextCourse);

    public function checkCoursePoint($user);
}
