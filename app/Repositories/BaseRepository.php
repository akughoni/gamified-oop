<?php

namespace App\Repositories;

use App\Models\Course;
use App\Models\Level;
use App\Models\TakeCourse;
use App\Models\TakeMission;
use App\Models\UserPoint;

class BaseRepository implements BaseContract
{
    public function setUserPoint($userId, $from, $fromId, $point)
    {
        return UserPoint::updateOrCreate([
            "user_id"  => $userId,
            "from"     => $from,
            "from_id"  => $fromId,
        ], [
            "point"    => $point
        ]);
    }

    public function getCourseByLevel($level)
    {
        return Course::where("level", $level)->first();
    }

    public function createDataTakeCourse($user, $courseId = null)
    {
        $attribute["user_id"] = $user["id"];
        $attribute["course_id"] = ($courseId == null) ? $this->getCourseByLevel(1)->id : $courseId;
        return TakeCourse::firstOrCreate($attribute);
    }

    
    public function createDataMission($user, $missionId = null)
    {
        $attribute["user_id"] = $user["id"];
        $attribute["mission_id"] = ($missionId == null) ? 1 : $missionId;
        return TakeMission::firstOrCreate($attribute);
    }

    public function createDataLevel($user)
    {
        $attribute["user_id"] = $user["id"];
        $level = Level::create($attribute);
        return $level;
    }

    public function createDataInit($user, $courseId = null, $missionId =null)
    {
        $this->createDataTakeCourse($user, $courseId);
        $this->createDataMission($user, $missionId);
        $this->createDataLevel($user);
    }

    public function userPoint($id)
    {
        $point = UserPoint::where("user_id", $id)->get();
        return $point->sum("point");
    }
}
