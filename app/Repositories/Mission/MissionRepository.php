<?php

namespace App\Repositories\Mission;

use App\Models\Mission;
use App\Models\TakeMission;
use App\Models\User;
use App\Repositories\BaseRepository;

class MissionRepository extends BaseRepository implements MissionContract
{
    public function userMission()
    {
        return User::with("takeMission")->with("missionTaken")->get()->find(auth()->user()->id);
    }

    public function missionNotTaken($user)
    {
        $array = $user->missionTaken->map(function ($mission) {
            return $mission->id;
        });
        return Mission::whereNotIn("id", $array)->get();
    }

    public function missionById($id)
    {
        return Mission::find($id);
    }

    public function updateTakeMissionIsDone($data)
    {
        $update = TakeMission::where($data)->first()->update(["is_done" => 1 ]);
        $this->setNextMissionOpen();
        return $update;
    }

    public function setNextMissionOpen()
    {
        $this->createDataMission(auth()->user(), $this->getNextMissionId());
    }

    public function getNextMissionId()
    {
        $user = User::with("takeMission")->with("missionTaken")->get()->find(auth()->user()->id);
        return Mission::whereNotIn("id", $this->getMissionTakenIdByUser($user))->orderBy("id", "ASC")->first()->id;
    }

    public function getMissionTakenIdByUser($user)
    {
        $array = $user->takeMission->filter(function ($takeMission) {
            return $takeMission->is_done == 1;
        });
        $array = $array->map(function ($takeMission) {
            return $takeMission->mission_id;
        });
        return $array;
    }
}
