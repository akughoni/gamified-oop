<?php

namespace App\Repositories\Mission;

use App\Repositories\BaseContract;

interface MissionContract extends BaseContract
{
    public function userMission();
    public function missionNotTaken($user);
    public function missionById($id);
    public function updateTakeMissionIsDone($data);
    public function setNextMissionOpen();
    public function getNextMissionId();
    public function getMissionTakenIdByUser($user);
}
