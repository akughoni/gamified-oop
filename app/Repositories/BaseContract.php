<?php

namespace App\Repositories;

interface BaseContract
{
    public function setUserPoint($userId, $from, $fromId, $point);

    public function createDataInit($user, $courseId =null, $missionId = null);
    public function createDataTakeCourse($user, $courseId = null);
    public function createDataMission($user, $missionId = null);
    public function createDataLevel($user);
    public function getCourseByLevel($level);
    public function userPoint($id);
}
