<?php

namespace App\Repositories\Course;

interface CourseContract
{
    public function userCourse();

    public function courseNotTaken($user);

    public function pointFromCourse($id);

    public function updateTakeCourseIsDone($userId, $courseId);

    public function courseTakenIdIsDone($user);
}
