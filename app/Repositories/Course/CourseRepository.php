<?php

namespace App\Repositories\Course;

use App\Models\Course;
use App\Models\Stage;
use App\Models\TakeCourse;
use App\Models\User;
use App\Repositories\BaseRepository;

class CourseRepository extends BaseRepository implements CourseContract
{
    public function courseNotTaken($user)
    {
        $arrayId = $user->coursesTaken->map(function ($course) {
            return $course->id;
        });
        return Course::whereNotIn("id", $arrayId)->get();
    }

    public function courseTakenIdIsDone($user)
    {
        $array = $user->takeCourse->filter(function ($takeCourse) {
            return $takeCourse->is_done == 1;
        });
        $array = $array->map(function ($takeCourse) {
            return $takeCourse->course_id;
        });
        return $array;
    }

    public function userCourse()
    {
        return User::with("takeCourse")->with("coursesTaken")->find(auth()->user()->id);
    }

    public function pointFromCourse($id)
    {
        $basePoint = 7;
        $stageAmount = Stage::where("course_id", $id)->get()->count();
        return $stageAmount * $basePoint;
    }

    public function updateTakeCourseIsDone($userId, $courseId)
    {
        return TakeCourse::where("course_id", $courseId)->where("user_id", $userId)->first()->update(["is_done" => 1 ]);
    }
}
