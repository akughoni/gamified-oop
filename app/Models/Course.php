<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Course extends Model
{
    protected $guarded = [];
    // protected $appends = ["image_url"];

    // public function getImageUrlAttribute()
    // {
    //     return Storage::url($this->image);
    // }

    public function stages()
    {
        return $this->hasMany(Stage::class);
    }

    public function quizs()
    {
        return $this->hasMany(Quiz::class);
    }
}
