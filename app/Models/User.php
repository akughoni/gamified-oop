<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'username', 'password','class','phone','image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ["amount"];

    public function level()
    {
        return $this->hasOne(Level::class);
    }

    public function points()
    {
        return $this->hasMany(UserPoint::class);
    }

    public function scopePoint($query)
    {
        return $query->points->sum("point");
    }

    public function missions()
    {
        return $this->belongsToMany(Mission::class, "take_missions");
    }

    public function badges()
    {
        return $this->hasManyThrough(Badge::class, TakeMission::class, "user_id", "mission_id", "id", "mission_id");
    }

    public function coursesTaken()
    {
        return $this->belongsToMany(Course::class, "take_courses");
    }

    public function takeCourse()
    {
        return $this->hasMany(TakeCourse::class);
    }

    public function missionTaken()
    {
        return $this->belongsToMany(Mission::class, "take_missions");
    }

    public function takeMission()
    {
        return $this->hasMany(TakeMission::class);
    }
}
