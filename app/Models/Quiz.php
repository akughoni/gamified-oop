<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $guarded = [];

    public function scopeQuestion($query, $courseId)
    {
        return $query->where("course_id", $courseId);
    }
}
