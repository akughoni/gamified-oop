<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TakeMission extends Model
{
    protected $guarded = [];

    public function mission()
    {
        return $this->belongsTo(Mission::class);
    }
}
