# #GAMIFIED OOP

 ## Langkah CLone Project ##
 ---
 ##### 1. Clone project dengan perintah.
   ` git clone https://gitlab.com/akughoni/gamified-oop.git`
 ##### 2. Jalankan perintah composer untuk instalasi package laravel
 `composer update`
  ##### 3. Siapkan database dengan nama bebas
 
  ##### 4. Copy .env.example melalui CLI 
 `cp .env.example .env`
    atau bisa secara manual dengan Copy Paste.
  ##### 5. Konfigurasi database pada .env
 
`DB_DATABASE=nama_database`
`DB_USERNAME=username_database`
`DB_PASSWORD=password_database`
 ##### 6. Jalankan migration beserta dengan Data Seeder (Data Dummy)
(jika pertama kali)
``` php artisan migrate --seed```
(jika mau reset database dan data dummy)
```php artisan migrate:fresh --seed```

 ##### 7. Tinggal Jalankan perintah artisan untuk run laravel
``` php artisan serve ```

## Langkah Update Project
---
1. Buka Code editor (VSCode), kemudian buka terminal atau bisa menggunakan CMD pada root project Gamified-OOP.
2. (Opsional) Cek dengan `git remote -v` apakah sudah ada remote dengan origin. seperti dibawah ini
```
origin  https://gitlab.com/akughoni/gamified-oop.git (fetch)
origin  https://gitlab.com/akughoni/gamified-oop.git (push)
```
3. (opsional )Jika sudah lanjut langkah berikutnya (Langkah 4), jika belum tambahkan remote dengan perintah
-- ` git remote add origin https://gitlab.com/akughoni/gamified-oop.git`
4. Jalankan perintah
 -- ```git pull origin master```
 5. Perlu diingat untuk tidak merubah code apapun. Namun jika diperlukan ketika merubah code yang ada diproject ini untuk menjalanakan perintah dibawah ini.
 -- `git add .`
-- `git commit -m "jelaskan perubahan apa didalam double quote ini"`
-- `git pull origin master`
-- `git push origin master`
(Jika terdapat perintah untuk memasukan username dan password gitlab. Silahkan isi dengan benar.)


### Terimakasih 